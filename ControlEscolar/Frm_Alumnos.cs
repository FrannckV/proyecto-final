﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolar;
using Enitades.ControlEscolar;

namespace ControlEscolar
{
    public partial class Frm_Alumnos : Form
    {
        AlumnoManejador _alumnoManejador;
        Alumnos _alumnos;
        public Frm_Alumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
        }
        private void BuscarAlumnos(string filtro)
        {
            Dtg_Datos.DataSource = _alumnoManejador.ObtenerLista(filtro);
        }

        private void Frm_Alumnos_Load(object sender, EventArgs e)
        {
            BuscarAlumnos("");
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumnos(txtBuscar.Text);
        }
        private void Eliminar()
        {
            string id = Dtg_Datos.CurrentRow.Cells["NControl"].Value.ToString();
            _alumnoManejador.Eliminar(id);
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    Eliminar();
                    BuscarAlumnos("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void BindingAlumno()
        {
            _alumnos.NControl = Dtg_Datos.CurrentRow.Cells["NControl"].Value.ToString();
            _alumnos.Nombre = Dtg_Datos.CurrentRow.Cells["Nombre"].Value.ToString();
            _alumnos.ApellidoPaterno = Dtg_Datos.CurrentRow.Cells["ApellidoPaterno"].Value.ToString();
            _alumnos.ApellidoMaterno = Dtg_Datos.CurrentRow.Cells["ApellidoMaterno"].Value.ToString();
            _alumnos.Sexo = Dtg_Datos.CurrentRow.Cells["Sexo"].Value.ToString();
            _alumnos.FechaNacimiento = Dtg_Datos.CurrentRow.Cells["FechaNacimiento"].Value.ToString();
            _alumnos.CorreoEletronico= Dtg_Datos.CurrentRow.Cells["CorreoEletronico"].Value.ToString();
            _alumnos.Telefono = Dtg_Datos.CurrentRow.Cells["Telefono"].Value.ToString();
            _alumnos.Estado= Dtg_Datos.CurrentRow.Cells["Estado"].Value.ToString();
            _alumnos.Municipio= Dtg_Datos.CurrentRow.Cells["Municipio"].Value.ToString();
            _alumnos.Domicilio= Dtg_Datos.CurrentRow.Cells["Domicilio"].Value.ToString();
        }

        private void Dtg_Datos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingAlumno();
            AlumnosModal alumnosModal= new AlumnosModal(_alumnos);
            alumnosModal.ShowDialog();
            BuscarAlumnos("");
        }

        private void Btn_nuevo_Click(object sender, EventArgs e)
        {
            AlumnosModal alumnosModal = new AlumnosModal();
            alumnosModal.ShowDialog();
            BuscarAlumnos("");
        }

        private void Dtg_Datos_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            BindingAlumno();
            AlumnosModal alumnosModal = new AlumnosModal(_alumnos);
            alumnosModal.ShowDialog();
            BuscarAlumnos("");
        }
    }
}
