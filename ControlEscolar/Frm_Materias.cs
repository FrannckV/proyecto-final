﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolar;
using Enitades.ControlEscolar;

namespace ControlEscolar
{
    public partial class Frm_Materias : Form
    {
        Materias _Materias;
        MateriaManejador _MateriaManejador;
        MateriasModal _MateriasModal;
        public Frm_Materias()
        {
            InitializeComponent();
            _Materias = new Materias();
            _MateriaManejador = new MateriaManejador();
        }
        private void BuscarMaterias(string filtro)
        {
            Dtg_Datos.DataSource = _MateriaManejador.ObtenerLista(filtro);
          //  Dtg_Datos.DataSource = _MateriaManejador.GetMaterias();
        }

        private void Btn_nuevo_Click(object sender, EventArgs e)
        {
            _MateriasModal = new MateriasModal(_Materias);
            _MateriasModal.ShowDialog();
            BuscarMaterias("");
        }

        private void Dtg_Datos_DoubleClick(object sender, EventArgs e)
        {
            BindingMateria();
            _MateriasModal = new MateriasModal(_Materias);
            _MateriasModal.ShowDialog();
            BuscarMaterias("");
        }

        private void Frm_Materias_Load(object sender, EventArgs e)
        {
            BuscarMaterias("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaterias(txtBuscar.Text);
        }
        private void Eliminar()
        {
            string id = Dtg_Datos.CurrentRow.Cells["idmateria"].Value.ToString();
            _MateriaManejador.Eliminar(id);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
            string error = _MateriaManejador.getError();
            if (!String.IsNullOrEmpty(error))
                MessageBox.Show(error);
            BuscarMaterias("");
        }
        private void BindingMateria()
        {
            _Materias.Idmateria = Dtg_Datos.CurrentRow.Cells["Idmateria"].Value.ToString();
            _Materias.Nombre = Dtg_Datos.CurrentRow.Cells["Nombre"].Value.ToString();
            _Materias.Anterior = Dtg_Datos.CurrentRow.Cells["Anterior"].Value.ToString();
            _Materias.Siguiente = Dtg_Datos.CurrentRow.Cells["Siguiente"].Value.ToString();
        }
    }
}
