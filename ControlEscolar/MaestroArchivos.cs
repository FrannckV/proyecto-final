﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Enitades.ControlEscolar;

namespace ControlEscolar
{
    public partial class MaestroArchivos : Form
    {
        Maestros _Maestros;
        string directorio = @"C:\ControlEscolar\Archivos\";
        string rutaTitulo = "", rutaMaestria = "", rutaDoctorado = "";
        int titulo = 0, maestria = 0, doctorado = 0;
        public MaestroArchivos(Maestros maestros)
        {
            InitializeComponent();
            _Maestros = new Maestros();
            _Maestros = maestros;
        }

        private void Btn_Aceptar_Click(object sender, EventArgs e)
        {
            titulo = 1;
            botones();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Archivo tipo(*.pdf)|*.pdf";
            //var _ruta = Application.StartupPath + "\\pdf\\";
            if (ofd.ShowDialog() == DialogResult.OK)
                Txt_Titulo.Text = ofd.FileName;
        }
        private void validarDirectorio()
        {
            if (!Directory.Exists(directorio)) //crear la carpeta en caso de que no exista.
                Directory.CreateDirectory(directorio);
            if (!Directory.Exists(directorio + _Maestros.NControl)) //crear la carpeta en caso de que no exista.
                Directory.CreateDirectory(directorio + _Maestros.NControl);
        }
        private void copiarTitulo()
        {
            string archivo;
            validarDirectorio();
            if (File.Exists(Txt_Titulo.Text))
            {
                if (titulo == 1)
                {
                    archivo = directorio + _Maestros.NControl + @"\Titulo" + _Maestros.NControl + ".pdf";
                    File.Copy(Txt_Titulo.Text, archivo, true);
                    _Maestros.DocTitulo = Path.GetFileName(archivo);
                }
            }
            else
                MessageBox.Show("Error. El archivo no existe o fue removido.");
            this.Close();
        }
        private void copiarMaestria()
        {
            string archivo;
            if (File.Exists(Txt_Maestria.Text))
            {
                if (maestria == 1)
                {
                    archivo = directorio + _Maestros.NControl + @"\Maestria" + _Maestros.NControl + ".pdf";
                    File.Delete(archivo);
                    File.Copy(Txt_Maestria.Text, archivo, true);
                    _Maestros.DocMaestria = Path.GetFileName(archivo);
                }
            }
            else
                MessageBox.Show("Error. El archivo no existe o fue removido.");
            this.Close();
        }
        private void copiarDoctorado()
        {
            string archivo;
            if (File.Exists(Txt_Doctorado.Text))
            {
                if (doctorado == 1)
                {
                    archivo = directorio + _Maestros.NControl + @"\Doctorado" + _Maestros.NControl + ".pdf";
                    File.Delete(archivo);
                    File.Copy(Txt_Doctorado.Text, archivo, true);
                    _Maestros.DocDoctorado = Path.GetFileName(archivo);
                }
            }
            else
                MessageBox.Show("Error. El archivo no existe o fue removido.");
            this.Close();
        }
        private void GuardarEliminar(Maestros maestros)
        {
            if ((maestros.DocTitulo == null || maestros.DocTitulo == "") && Txt_Titulo.Text == "")
                this.Close();
            else if ((maestros.DocTitulo != null || maestros.DocTitulo != "") && Txt_Titulo.Text == "")
            {
                EliminarArchivo(rutaTitulo);
                _Maestros.DocTitulo = "";
            }
            else
                copiarTitulo();
            if ((maestros.DocMaestria == null || maestros.DocMaestria == "") && Txt_Maestria.Text == "")
                this.Close();
            else if ((maestros.DocMaestria != null || maestros.DocMaestria != "") && Txt_Maestria.Text == "")
            {
                EliminarArchivo(rutaMaestria);
                _Maestros.DocMaestria = "";
            }
            else
                copiarMaestria();
            if ((maestros.DocDoctorado == null || maestros.DocDoctorado == "") && Txt_Doctorado.Text == "")
                this.Close();
            else if ((maestros.DocDoctorado != null || maestros.DocDoctorado != "") && Txt_Doctorado.Text == "")
            {
                EliminarArchivo(rutaDoctorado);
                _Maestros.DocDoctorado = "";
            }
            else
                copiarDoctorado();
        }
        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            GuardarEliminar(_Maestros);
            this.Close();
        }

        private void MaestroArchivos_Load(object sender, EventArgs e)
        {
            Btn_Guardar.Enabled = false;
            if (_Maestros.DocTitulo != null)
            {
                if (_Maestros.DocTitulo != "")
                    Txt_Titulo.Text = directorio + _Maestros.NControl + @"\" + _Maestros.DocTitulo;
            }
            if (_Maestros.DocMaestria != null)
            {
                if (_Maestros.DocMaestria != "")
                    Txt_Maestria.Text = directorio + _Maestros.NControl + @"\" + _Maestros.DocMaestria;
            }
            if (_Maestros.DocDoctorado != null)
            {
                if (_Maestros.DocDoctorado != "")
                    Txt_Doctorado.Text = directorio + _Maestros.NControl + @"\" + _Maestros.DocDoctorado;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Btn_Guardar.Enabled == true)
            {
                if (MessageBox.Show("¿Estás seguro que deseas salir sin guardar?", "Salir sin guardar", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    this.Close();
            }
            else
                this.Close();
        }

        private void Btn_Maestria_Click(object sender, EventArgs e)
        {
            maestria = 1;
            botones();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Imagen tipo(*.pdf)|*.pdf";
            //var _ruta = Application.StartupPath + "\\pdf\\";
            if (ofd.ShowDialog() == DialogResult.OK)
                Txt_Maestria.Text = ofd.FileName;
        }

        private void Btn_Doctorado_Click(object sender, EventArgs e)
        {
            doctorado = 1;
            botones();
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Imagen tipo(*.pdf)|*.pdf";
            //var _ruta = Application.StartupPath + "\\pdf\\";
            if (ofd.ShowDialog() == DialogResult.OK)
                Txt_Doctorado.Text = ofd.FileName;
        }
        private void EliminarArchivo(string txt)
        {
            if (File.Exists(txt))
            {
                File.Delete(txt);
            }
            else
                MessageBox.Show("Error, el archivo no se ha encontrado o ha sido removido de la carpeta fuente.");
        }
        private void EliminarTitulo(TextBox txt)
        {
            if (MessageBox.Show("¿Está seguro que desea eliminar el archivo?", "Eliminar documento", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                rutaTitulo = txt.Text;
                txt.Clear();
            }
        }
        private void EliminarMaestria(TextBox txt)
        {
            if (MessageBox.Show("¿Está seguro que desea eliminar el archivo?", "Eliminar documento", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                rutaMaestria = txt.Text;
                txt.Clear();
            }
        }
        private void EliminarDoctorado(TextBox txt)
        {
            if (MessageBox.Show("¿Está seguro que desea eliminar el archivo?", "Eliminar documento", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                rutaDoctorado = txt.Text;
                txt.Clear();
            }
        }

        private void Btn_EliminarTitulo_Click(object sender, EventArgs e)
        {
            botones();
            EliminarTitulo(Txt_Titulo);
        }

        private void Btn_EliminarMaestria_Click(object sender, EventArgs e)
        {
            botones();//  Eliminar(Txt_Maestria);
            EliminarMaestria(Txt_Maestria);
        }

        private void Btn_EliminarDoctorado_Click(object sender, EventArgs e)
        {
            botones();//Eliminar(Txt_Doctorado);
            EliminarDoctorado(Txt_Doctorado);
        }
        private void botones()
        {
            Btn_Guardar.Enabled = true;
        }
        private void Txt_Titulo_TextChanged(object sender, EventArgs e)
        {
        }

        private void Txt_Maestria_TextChanged(object sender, EventArgs e)
        {
        }

        private void Txt_Doctorado_TextChanged(object sender, EventArgs e)
        {
        }
    }
}
