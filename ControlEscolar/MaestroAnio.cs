﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using System.Text.RegularExpressions;

namespace ControlEscolar
{
    public partial class MaestroAnio : Form
    {
        Maestros _Maestros;
        public MaestroAnio(Maestros maestros)
        {
            InitializeComponent();
            _Maestros = new Maestros();
            _Maestros = maestros;
        }

        private void Txt_ApellidoMaterno_TextChanged(object sender, EventArgs e)
        {
            Btn_Aceptar.Enabled = true;
        }

        private void Btn_Aceptar_Click(object sender, EventArgs e)
        {
            if (AnioValido(Txt_Anio.Text))
            {
                _Maestros.Anio = Txt_Anio.Text;
                this.Close();
            }
            else
                MessageBox.Show("Ingrese un año válido");
        }
        private bool AnioValido(string anio)
        {
            var regex = new Regex(@"^[0-9]{4}$");
            var match = regex.Match(anio);
            if (match.Success)
                return true;
            else
                return false;
        }

    }
}
