﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class MateriasModal : Form
    {
        private bool _isEnableBinding = false;
        Materias _Materias;
        MateriaManejador _MateriaManejador;
        public MateriasModal(Materias materias)
        {
            InitializeComponent();
            _MateriaManejador = new MateriaManejador();
            _Materias = new Materias();
            //materias = _MateriaManejador.GetMaterias();
            BuscarMaterias();
            _Materias = materias;
            if (!string.IsNullOrEmpty(materias.Idmateria))
                LoadEntity();
            _isEnableBinding = true;
        }
        /*private void ValidaCmb()
        {
            if (!string.IsNullOrEmpty(_Materias.Anterior))
                BuscarMaterias(Cmb_Anterior);
            else
                BuscarMaterias("");
            if (!string.IsNullOrEmpty(_Materias.Siguiente))
                BuscarMaterias(Cmb_Siguiente);
        }*/
        private void LoadEntity()
        {
            Txt_Nombre.Text = _Materias.Nombre;
            Cmb_Anterior.SelectedItem= _Materias.Anterior;
            Cmb_Siguiente.SelectedItem = _Materias.Siguiente;
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
        private bool validarMateria()
        {
            var res = _MateriaManejador.EsMateriaValida(_Materias);
            if (!res.Item1) //Diferente de true
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            if (validarMateria())
            {
                Guardar();
                _MateriaManejador.VaciarMaterias(_Materias);
                this.Close();
            }
        }
        private void Guardar()
        {
            _MateriaManejador.Guardar(_Materias);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de eliminar el logo? Los cambios se efectuarán después de guardar", "Eliminar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                this.Close();
            _MateriaManejador.VaciarMaterias(_Materias);
        }
        private void BindEntity()
        {
            if (_isEnableBinding)
            {
                _Materias.Nombre = Txt_Nombre.Text;
                _Materias.Anterior = Cmb_Anterior.SelectedValue.ToString();
                _Materias.Siguiente = Cmb_Siguiente.SelectedValue.ToString();
            }
        }
        private void BuscarMaterias()
        {
            Cmb_Anterior.DataSource = _MateriaManejador.ObtenerLista("");
            Cmb_Siguiente.DataSource = _MateriaManejador.ObtenerLista("");
            Cmb_Anterior.DisplayMember = "nombre";
            Cmb_Siguiente.DisplayMember = "nombre";
            Cmb_Anterior.ValueMember = "idmateria";
            Cmb_Siguiente.ValueMember = "idmateria";
        }
        private void Txt_Nombre_TextChanged(object sender, EventArgs e)
        {
            BindEntity();
        }

        private void Cmb_Anterior_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEntity();
        }

        private void Cmb_Siguiente_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEntity();
        }

        private void MateriasModal_Load(object sender, EventArgs e)
        {

        }

        private void Txt_Nombre_Leave(object sender, EventArgs e)
        {
            BindEntity();
        }
    }
}
