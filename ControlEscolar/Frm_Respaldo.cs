﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class Frm_Respaldo : Form
    {
        string _directorioDestino;
        public Frm_Respaldo()
        {
            InitializeComponent();
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {

        }
        private void SelectPath()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                DialogResult result = dialog.ShowDialog();
                if(result==DialogResult.OK&& !string.IsNullOrEmpty(dialog.SelectedPath))
                {
                    _directorioDestino = dialog.SelectedPath;
                    textBox1.Text = _directorioDestino;
                    Btn_Guardar.Enabled = true;
                }
            }
        }
    }
}
