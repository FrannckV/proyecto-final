﻿namespace ControlEscolar
{
    partial class EscuelaApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Nombre = new System.Windows.Forms.TextBox();
            this.Txt_Director = new System.Windows.Forms.TextBox();
            this.Pic_Logo = new System.Windows.Forms.PictureBox();
            this.Btn_Modificar = new System.Windows.Forms.Button();
            this.Btn_Guardar = new System.Windows.Forms.Button();
            this.Btn_EliminaLogo = new System.Windows.Forms.Button();
            this.Btn_AgregaLogo = new System.Windows.Forms.Button();
            this.Btn_Cancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(32, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(32, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Director:";
            // 
            // Txt_Nombre
            // 
            this.Txt_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Txt_Nombre.Location = new System.Drawing.Point(36, 54);
            this.Txt_Nombre.Name = "Txt_Nombre";
            this.Txt_Nombre.Size = new System.Drawing.Size(290, 26);
            this.Txt_Nombre.TabIndex = 2;
            this.Txt_Nombre.TextChanged += new System.EventHandler(this.Txt_Nombre_TextChanged);
            // 
            // Txt_Director
            // 
            this.Txt_Director.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Txt_Director.Location = new System.Drawing.Point(36, 127);
            this.Txt_Director.Name = "Txt_Director";
            this.Txt_Director.Size = new System.Drawing.Size(290, 26);
            this.Txt_Director.TabIndex = 3;
            this.Txt_Director.TextChanged += new System.EventHandler(this.Txt_Director_TextChanged);
            // 
            // Pic_Logo
            // 
            this.Pic_Logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Pic_Logo.Location = new System.Drawing.Point(67, 166);
            this.Pic_Logo.Name = "Pic_Logo";
            this.Pic_Logo.Size = new System.Drawing.Size(199, 184);
            this.Pic_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Pic_Logo.TabIndex = 4;
            this.Pic_Logo.TabStop = false;
            // 
            // Btn_Modificar
            // 
            this.Btn_Modificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_Modificar.Location = new System.Drawing.Point(25, 369);
            this.Btn_Modificar.Name = "Btn_Modificar";
            this.Btn_Modificar.Size = new System.Drawing.Size(102, 37);
            this.Btn_Modificar.TabIndex = 5;
            this.Btn_Modificar.Text = "Modificar";
            this.Btn_Modificar.UseVisualStyleBackColor = true;
            // 
            // Btn_Guardar
            // 
            this.Btn_Guardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_Guardar.Location = new System.Drawing.Point(133, 369);
            this.Btn_Guardar.Name = "Btn_Guardar";
            this.Btn_Guardar.Size = new System.Drawing.Size(103, 37);
            this.Btn_Guardar.TabIndex = 6;
            this.Btn_Guardar.Text = "Guardar";
            this.Btn_Guardar.UseVisualStyleBackColor = true;
            this.Btn_Guardar.Click += new System.EventHandler(this.Btn_Guardar_Click);
            // 
            // Btn_EliminaLogo
            // 
            this.Btn_EliminaLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_EliminaLogo.Location = new System.Drawing.Point(272, 211);
            this.Btn_EliminaLogo.Name = "Btn_EliminaLogo";
            this.Btn_EliminaLogo.Size = new System.Drawing.Size(54, 37);
            this.Btn_EliminaLogo.TabIndex = 7;
            this.Btn_EliminaLogo.Text = "X";
            this.Btn_EliminaLogo.UseVisualStyleBackColor = true;
            this.Btn_EliminaLogo.Click += new System.EventHandler(this.Btn_EliminaLogo_Click);
            // 
            // Btn_AgregaLogo
            // 
            this.Btn_AgregaLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_AgregaLogo.Location = new System.Drawing.Point(272, 168);
            this.Btn_AgregaLogo.Name = "Btn_AgregaLogo";
            this.Btn_AgregaLogo.Size = new System.Drawing.Size(54, 37);
            this.Btn_AgregaLogo.TabIndex = 8;
            this.Btn_AgregaLogo.Text = "...";
            this.Btn_AgregaLogo.UseVisualStyleBackColor = true;
            this.Btn_AgregaLogo.Click += new System.EventHandler(this.Btn_AgregaLogo_Click);
            // 
            // Btn_Cancelar
            // 
            this.Btn_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.Btn_Cancelar.Location = new System.Drawing.Point(242, 369);
            this.Btn_Cancelar.Name = "Btn_Cancelar";
            this.Btn_Cancelar.Size = new System.Drawing.Size(105, 37);
            this.Btn_Cancelar.TabIndex = 9;
            this.Btn_Cancelar.Text = "Cancelar";
            this.Btn_Cancelar.UseVisualStyleBackColor = true;
            this.Btn_Cancelar.Click += new System.EventHandler(this.Btn_Cancelar_Click);
            // 
            // EscuelaApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 418);
            this.Controls.Add(this.Btn_Cancelar);
            this.Controls.Add(this.Btn_AgregaLogo);
            this.Controls.Add(this.Btn_EliminaLogo);
            this.Controls.Add(this.Btn_Guardar);
            this.Controls.Add(this.Btn_Modificar);
            this.Controls.Add(this.Pic_Logo);
            this.Controls.Add(this.Txt_Director);
            this.Controls.Add(this.Txt_Nombre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "EscuelaApp";
            this.Text = "EscuelaApp";
            this.Load += new System.EventHandler(this.EscuelaApp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Txt_Nombre;
        private System.Windows.Forms.TextBox Txt_Director;
        private System.Windows.Forms.PictureBox Pic_Logo;
        private System.Windows.Forms.Button Btn_Modificar;
        private System.Windows.Forms.Button Btn_Guardar;
        private System.Windows.Forms.Button Btn_EliminaLogo;
        private System.Windows.Forms.Button Btn_AgregaLogo;
        private System.Windows.Forms.Button Btn_Cancelar;
    }
}