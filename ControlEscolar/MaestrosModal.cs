﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class MaestrosModal : Form
    {
        MaestroManejador _MaestroManejador;
        Maestros _Maestros;
        EstadosManejador _EstadosManejador;
        MunicipiosManejador _MunicipiosManejador;
        public MaestrosModal()
        {
            InitializeComponent();
            _MaestroManejador = new MaestroManejador();
            _Maestros = new Maestros();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            BuscarEstados("");
            Btn_Doctos.Visible = false;
        }
        public MaestrosModal(Maestros maestros)
        {
            InitializeComponent();
            _MaestroManejador = new MaestroManejador();
            _EstadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _Maestros = new Maestros();
            _Maestros = maestros;
            BuscarEstados("");
            BindingMaestroReload();
        }
        private void BuscarEstados(string filtro)
        {
            Cmb_Estado.DataSource = _EstadosManejador.ObtenerLista(filtro);
            Cmb_Estado.DisplayMember = "nombre";
            Cmb_Estado.ValueMember = "codigo";
        }

        private void BuscarMunicipios(string filtro)
        {
            Cmb_Municipio.DataSource = _MunicipiosManejador.ObtenerLista(filtro);
            Cmb_Municipio.DisplayMember = "nombre";
            Cmb_Municipio.ValueMember = "idm";
        }
        private bool ValidarMaestro()
        {
            var res = _MaestroManejador.EsMaestroValido(_Maestros);
            if (!res.Item1) //Diferente de true
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            BindingMaestro();
            if (ValidarMaestro())
            {
                Guardar();
                Guardar();
                this.Close();
            }
        }
        private void Guardar()
        {
            int c = 0;
            if (_Maestros.NControl == null)
            {
                MaestroAnio maestroAnio = new MaestroAnio(_Maestros);
                maestroAnio.ShowDialog();
                c = 1;
            }
            _MaestroManejador.Guardar(_Maestros, "");
            if (c == 1)
            {
                if (MessageBox.Show("¿Desea agregar ahora los documentos para el usuario " + _Maestros.Nombre + "?", "Agregar archivos", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    button1.Enabled = false;
                    MaestroArchivos maestroArchivos = new MaestroArchivos(_Maestros);
                    maestroArchivos.ShowDialog();
                }
            }
        }
        private void BindingMaestroReload()
        {
            Txt_Nombre.Text = _Maestros.Nombre;
            Txt_ApellidoPaterno.Text = _Maestros.Apellidopaterno;
            Txt_ApellidoMaterno.Text = _Maestros.Apellidomaterno;
            Cmb_Sexo.Text = _Maestros.Sexo;
            Dtp_FechaNacimiento.Text = _Maestros.FechaNacimiento;
            Txt_Correo.Text = _Maestros.CorreoElectronico;
            Txt_nTarjeta.Text = _Maestros.Tarjeta;
            Cmb_Estado.SelectedItem = _Maestros.Esta;
            Cmb_Municipio.SelectedItem = _Maestros.Municipio;
        }
        private void BindingMaestro()
        {
            if (_Maestros.NControl == null)
            {
                _Maestros.NControl = null; //Generar número de control
            }
            _Maestros.Nombre = Txt_Nombre.Text;
            _Maestros.Apellidopaterno = Txt_ApellidoPaterno.Text;
            _Maestros.Apellidomaterno = Txt_ApellidoMaterno.Text;
            _Maestros.Sexo = Cmb_Sexo.Text;
            _Maestros.FechaNacimiento = Dtp_FechaNacimiento.Text;
            _Maestros.CorreoElectronico = Txt_Correo.Text;
            _Maestros.Tarjeta= Txt_nTarjeta.Text;
            _Maestros.Esta = Cmb_Estado.Text;
            _Maestros.Municipio = Cmb_Municipio.Text;
        }

        private void Cmb_Estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuscarMunicipios("where fkestado='" + Cmb_Estado.SelectedValue + "'");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Btn_Doctos_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            MaestroArchivos maestroArchivos = new MaestroArchivos(_Maestros);
            maestroArchivos.ShowDialog();
            Btn_Doctos.Enabled = false;
        }

        private void MaestrosModal_Load(object sender, EventArgs e)
        {

        }
    }
}
