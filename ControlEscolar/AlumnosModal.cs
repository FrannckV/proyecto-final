﻿using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class AlumnosModal : Form
    {
        private AlumnoManejador _alumnoManejador;
        private Alumnos _alumnos;
        private EstadosManejador _estadosManejador;
        private MunicipiosManejador _MunicipiosManejador;
        private bool _isEnableBinding = false;
        public AlumnosModal()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _alumnos = new Alumnos();
            _estadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            BuscarEstados("");
            _isEnableBinding = true;
            //BindingAlumno();
            //_alumnos.NControl = "";

        }
        public AlumnosModal(Alumnos alumnos)
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _estadosManejador = new EstadosManejador();
            _MunicipiosManejador = new MunicipiosManejador();
            _alumnos = new Alumnos();
            _alumnos = alumnos;
            BuscarEstados("");
            BindingAlumnoReload();
            _isEnableBinding = true;
        }
        private void BuscarEstados(string filtro)
        {
            Cmb_Estado.DataSource = _estadosManejador.ObtenerLista(filtro);
            Cmb_Estado.DisplayMember = "nombre";
            Cmb_Estado.ValueMember = "codigo";
        }
        private void BuscarMunicipios(string filtro)
        {
            Cmb_Municipio.DataSource = _MunicipiosManejador.ObtenerLista(filtro);
            Cmb_Municipio.DisplayMember = "nombre";
            Cmb_Municipio.ValueMember = "idm";
        }
        private bool ValidarAlumno()
        {
            var res = _alumnoManejador.EsAlumnoValido(_alumnos);
            if (!res.Item1) //Diferente de true
            {
                MessageBox.Show(res.Item2);
            }
            return res.Item1;
        }
        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            BindingAlumno();
            if (ValidarAlumno())
            {
                Guardar();
                this.Close();
            }
        }
        private void Guardar()
        {
            _alumnoManejador.Guardar(_alumnos);
        }
        private void BindingAlumnoReload()
        {
            Txt_Nombre.Text = _alumnos.Nombre;
            Txt_ApellidoPaterno.Text = _alumnos.ApellidoPaterno;
            Txt_ApellidoMaterno.Text = _alumnos.ApellidoMaterno;
            Cmb_Sexo.Text = _alumnos.Sexo;
            Dtp_FechaNacimiento.Text = _alumnos.FechaNacimiento;
            Txt_Correo.Text = _alumnos.CorreoEletronico;
            Txt_Telefono.Text = _alumnos.Telefono;
            Cmb_Estado.Text = _alumnos.Estado;
            Cmb_Municipio.Text = _alumnos.Municipio;
            Txt_Domicilio.Text = _alumnos.Domicilio;
        }
        private void BindingAlumno()
        {
            if (_isEnableBinding)
            {
                if (_alumnos.NControl == null)
                {
                    _alumnos.NControl = null; //Generar número de control
                }
                _alumnos.Nombre = Txt_Nombre.Text;
                _alumnos.ApellidoPaterno = Txt_ApellidoPaterno.Text;
                _alumnos.ApellidoMaterno = Txt_ApellidoMaterno.Text;
                _alumnos.Sexo = Cmb_Sexo.Text;
                _alumnos.FechaNacimiento = Dtp_FechaNacimiento.Text;
                _alumnos.CorreoEletronico = Txt_Correo.Text;
                _alumnos.Telefono = Txt_Telefono.Text;
                _alumnos.Estado = Cmb_Estado.Text;
                _alumnos.Municipio = Cmb_Municipio.Text;
                _alumnos.Domicilio = Txt_Domicilio.Text;
            }
        }

        private void Txt_Nombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void AlumnosModal_Load(object sender, EventArgs e)
        {
            //Cmb_Estado.SelectedValue = 1;
        }

        private void Cmb_Estado_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Cmb_Municipio.DataSource = "";
            BuscarMunicipios( "where fkestado='" + Cmb_Estado.SelectedValue + "'");
        }

        private void Cmb_Estado_TextChanged(object sender, EventArgs e)
        {
            BuscarMunicipios("where fkestado='" + Cmb_Estado.SelectedValue + "'");
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
