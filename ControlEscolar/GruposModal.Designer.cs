﻿namespace ControlEscolar
{
    partial class GruposModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.Dtg_AlumnoGrupo = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.Dtg_Alumnos = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Cmb_Carrera = new System.Windows.Forms.ComboBox();
            this.Cmb_Grupo = new System.Windows.Forms.ComboBox();
            this.Cmb_Grado = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Btn_Cancelar = new System.Windows.Forms.Button();
            this.Btn_Guardar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.Dtg_GrupoMateria = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.Dtg_Materias = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_AlumnoGrupo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_Alumnos)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_GrupoMateria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_Materias)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.Dtg_AlumnoGrupo);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.Dtg_Alumnos);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(12, 85);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(640, 239);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lista de alumnos";
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(278, 103);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(93, 50);
            this.button3.TabIndex = 1;
            this.button3.Text = "🡠";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(278, 47);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(93, 50);
            this.button2.TabIndex = 0;
            this.button2.Text = "🡢";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Dtg_AlumnoGrupo
            // 
            this.Dtg_AlumnoGrupo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dtg_AlumnoGrupo.Location = new System.Drawing.Point(377, 47);
            this.Dtg_AlumnoGrupo.Name = "Dtg_AlumnoGrupo";
            this.Dtg_AlumnoGrupo.Size = new System.Drawing.Size(253, 186);
            this.Dtg_AlumnoGrupo.TabIndex = 22;
            this.Dtg_AlumnoGrupo.DoubleClick += new System.EventHandler(this.Dtg_AlumnoGrupo_DoubleClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(464, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 20);
            this.label5.TabIndex = 21;
            this.label5.Text = "En el grupo:";
            // 
            // Dtg_Alumnos
            // 
            this.Dtg_Alumnos.AllowUserToAddRows = false;
            this.Dtg_Alumnos.AllowUserToDeleteRows = false;
            this.Dtg_Alumnos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dtg_Alumnos.Location = new System.Drawing.Point(19, 47);
            this.Dtg_Alumnos.Name = "Dtg_Alumnos";
            this.Dtg_Alumnos.ReadOnly = true;
            this.Dtg_Alumnos.Size = new System.Drawing.Size(253, 186);
            this.Dtg_Alumnos.TabIndex = 20;
            this.Dtg_Alumnos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dtg_Alumnos_CellContentClick);
            this.Dtg_Alumnos.DoubleClick += new System.EventHandler(this.Dtg_Alumnos_DoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(109, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 20);
            this.label4.TabIndex = 19;
            this.label4.Text = "General:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Cmb_Carrera);
            this.groupBox1.Controls.Add(this.Cmb_Grupo);
            this.groupBox1.Controls.Add(this.Cmb_Grado);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(641, 67);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Información general";
            // 
            // Cmb_Carrera
            // 
            this.Cmb_Carrera.BackColor = System.Drawing.Color.White;
            this.Cmb_Carrera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Carrera.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Carrera.FormattingEnabled = true;
            this.Cmb_Carrera.Items.AddRange(new object[] {
            "Sistemas Computacionales",
            "Sistemas Automotrices",
            "Industrial",
            "Civil",
            "Gestión empresarial",
            "Electromecánica"});
            this.Cmb_Carrera.Location = new System.Drawing.Point(386, 28);
            this.Cmb_Carrera.Name = "Cmb_Carrera";
            this.Cmb_Carrera.Size = new System.Drawing.Size(244, 28);
            this.Cmb_Carrera.TabIndex = 2;
            this.Cmb_Carrera.SelectedIndexChanged += new System.EventHandler(this.Cmb_Carrera_SelectedIndexChanged);
            // 
            // Cmb_Grupo
            // 
            this.Cmb_Grupo.BackColor = System.Drawing.Color.White;
            this.Cmb_Grupo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Grupo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Grupo.FormattingEnabled = true;
            this.Cmb_Grupo.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"});
            this.Cmb_Grupo.Location = new System.Drawing.Point(235, 28);
            this.Cmb_Grupo.Name = "Cmb_Grupo";
            this.Cmb_Grupo.Size = new System.Drawing.Size(77, 28);
            this.Cmb_Grupo.TabIndex = 1;
            this.Cmb_Grupo.SelectedIndexChanged += new System.EventHandler(this.Cmb_Grupo_SelectedIndexChanged);
            // 
            // Cmb_Grado
            // 
            this.Cmb_Grado.BackColor = System.Drawing.Color.White;
            this.Cmb_Grado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Grado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Grado.FormattingEnabled = true;
            this.Cmb_Grado.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.Cmb_Grado.Location = new System.Drawing.Point(79, 28);
            this.Cmb_Grado.Name = "Cmb_Grado";
            this.Cmb_Grado.Size = new System.Drawing.Size(77, 28);
            this.Cmb_Grado.TabIndex = 0;
            this.Cmb_Grado.SelectedIndexChanged += new System.EventHandler(this.Cmb_Grado_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(318, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 20);
            this.label1.TabIndex = 9;
            this.label1.Text = "Carrera:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Grado:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(171, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 11;
            this.label3.Text = "Grupo:";
            // 
            // Btn_Cancelar
            // 
            this.Btn_Cancelar.FlatAppearance.BorderSize = 5;
            this.Btn_Cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Cancelar.ForeColor = System.Drawing.Color.White;
            this.Btn_Cancelar.Location = new System.Drawing.Point(537, 575);
            this.Btn_Cancelar.Name = "Btn_Cancelar";
            this.Btn_Cancelar.Size = new System.Drawing.Size(116, 65);
            this.Btn_Cancelar.TabIndex = 4;
            this.Btn_Cancelar.Text = "Cancelar";
            this.Btn_Cancelar.UseVisualStyleBackColor = true;
            this.Btn_Cancelar.Click += new System.EventHandler(this.Btn_Cancelar_Click);
            // 
            // Btn_Guardar
            // 
            this.Btn_Guardar.FlatAppearance.BorderSize = 5;
            this.Btn_Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Guardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Guardar.ForeColor = System.Drawing.Color.White;
            this.Btn_Guardar.Location = new System.Drawing.Point(408, 576);
            this.Btn_Guardar.Name = "Btn_Guardar";
            this.Btn_Guardar.Size = new System.Drawing.Size(116, 62);
            this.Btn_Guardar.TabIndex = 3;
            this.Btn_Guardar.Text = "Guardar";
            this.Btn_Guardar.UseVisualStyleBackColor = true;
            this.Btn_Guardar.Click += new System.EventHandler(this.Btn_Guardar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.Dtg_GrupoMateria);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.Dtg_Materias);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(13, 330);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(640, 239);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Lista de materias";
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(278, 103);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(93, 50);
            this.button4.TabIndex = 1;
            this.button4.Text = "🡠";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(278, 47);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(93, 50);
            this.button5.TabIndex = 0;
            this.button5.Text = "🡢";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Dtg_GrupoMateria
            // 
            this.Dtg_GrupoMateria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dtg_GrupoMateria.Location = new System.Drawing.Point(377, 47);
            this.Dtg_GrupoMateria.Name = "Dtg_GrupoMateria";
            this.Dtg_GrupoMateria.Size = new System.Drawing.Size(253, 186);
            this.Dtg_GrupoMateria.TabIndex = 22;
            this.Dtg_GrupoMateria.DoubleClick += new System.EventHandler(this.Dtg_GrupoMateria_DoubleClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(464, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 20);
            this.label6.TabIndex = 21;
            this.label6.Text = "En el grupo:";
            // 
            // Dtg_Materias
            // 
            this.Dtg_Materias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Dtg_Materias.Location = new System.Drawing.Point(19, 47);
            this.Dtg_Materias.Name = "Dtg_Materias";
            this.Dtg_Materias.Size = new System.Drawing.Size(253, 186);
            this.Dtg_Materias.TabIndex = 20;
            this.Dtg_Materias.DoubleClick += new System.EventHandler(this.Dtg_Materias_DoubleClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(109, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 20);
            this.label7.TabIndex = 19;
            this.label7.Text = "General:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(8, 568);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(355, 80);
            this.label8.TabIndex = 12;
            this.label8.Text = "Dé doble click en el alumno/materia que desee \r\nagregar oeliminar de la lista o h" +
    "aga uso de la \r\nbotonera dando un click sobre el alumno/materia \r\nprimero.";
            // 
            // GruposModal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(667, 650);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Btn_Cancelar);
            this.Controls.Add(this.Btn_Guardar);
            this.Name = "GruposModal";
            this.Text = "GruposModal";
            this.Load += new System.EventHandler(this.GruposModal_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_AlumnoGrupo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_Alumnos)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_GrupoMateria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dtg_Materias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox Cmb_Grado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Btn_Cancelar;
        private System.Windows.Forms.Button Btn_Guardar;
        private System.Windows.Forms.ComboBox Cmb_Carrera;
        private System.Windows.Forms.ComboBox Cmb_Grupo;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView Dtg_AlumnoGrupo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView Dtg_Alumnos;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView Dtg_GrupoMateria;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView Dtg_Materias;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}