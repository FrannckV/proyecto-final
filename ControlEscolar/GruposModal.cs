﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class GruposModal : Form
    {
        GrupoMateriaManejador _GrupoMateriaManejador;
        AlumnoGrupoManejador _AlumnoGrupoManejador;
        AlumnoManejador _AlumnoManejador;
        GrupoManejador _GrupoManejador;
        MateriaManejador _MateriaManejador;
        AlumnosGrupo _AlumnosGrupo;
        GrupoMaterias _GrupoMaterias;
        Grupos _Grupos;
        List<string> CommitAlumnosGrupo= new List<string>();
        List<string> CommitGrupoMateria= new List<string>();
        List<Alumnos> ListaAlumnos;
        List<Materias> ListaMaterias;
        List<Alumnos> ListaAlumnosGrupos;
        List<Materias> ListaGrupoMateria;
        private bool _isEnableBinding = false;
        string comando;

        public GruposModal(Grupos grupos)
        {
            InitializeComponent();
            _GrupoMateriaManejador = new GrupoMateriaManejador();
            _AlumnoGrupoManejador = new AlumnoGrupoManejador();
            _AlumnoManejador = new AlumnoManejador();
            _MateriaManejador = new MateriaManejador();
            _GrupoManejador = new GrupoManejador();
            _AlumnosGrupo = new AlumnosGrupo();
            _GrupoMaterias = new GrupoMaterias();
            _Grupos = grupos;
            ListaAlumnos = Alumnos();
            ListaMaterias = Materias();
            ListaAlumnosGrupos = AlumnosGrupo();
            ListaGrupoMateria = GrupoMaterias();
            if (grupos.Grupo >= 0 && !string.IsNullOrEmpty(grupos.Grado))
                LoadEntity();
            //   else
            //     BuscarGrupo(grupos);
            Cmb_Carrera.SelectedIndex = 0;
            Cmb_Grado.SelectedIndex = 0;
            Cmb_Grupo.SelectedIndex = 0;

            LlenarCombos();
            _isEnableBinding = true;
            RevisarDtg();
           }
        void RevisarDtg()
        {
            if (Dtg_Materias.RowCount == 0)
                Dtg_Alumnos.Enabled = false;
            else
                Dtg_Alumnos.Enabled = true;

        }
        private void LoadEntity()
        {
            Cmb_Carrera.SelectedItem=_Grupos.Carrera;
            Cmb_Grado.SelectedItem = _Grupos.Grupo;
            Cmb_Grupo.SelectedItem = _Grupos.Grado;
            Cmb_Grupo.Enabled = false;
            Cmb_Grado.Enabled = false;
            Cmb_Carrera.Enabled = false;
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            try
            {
                _GrupoManejador.Guardar(_Grupos);
                _GrupoMateriaManejador.EjecutaCommit(CommitGrupoMateria);
                _AlumnoGrupoManejador.EjecutaCommit(CommitAlumnosGrupo);
                MessageBox.Show("Datos guardados correctamente");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BuscarGrupo(Grupos grupos)
        {
            Cmb_Grupo.DataSource = _GrupoManejador.ObtenerLista(grupos,"");
            Cmb_Grado.DataSource = _GrupoManejador.ObtenerLista(grupos,"");
            Cmb_Carrera.DataSource = _GrupoManejador.ObtenerLista(grupos,"");
            Cmb_Grado.DisplayMember = "grupo";
            Cmb_Grupo.DisplayMember = "grado";
            Cmb_Carrera.DisplayMember = "carrera";
        }
        private void LlenarCombos()
        {
            Dtg_Alumnos.DataSource = null;
            Dtg_Alumnos.Refresh();
            Dtg_Alumnos.DataSource = ListaAlumnos;
            Dtg_Alumnos.Refresh();
            Dtg_Alumnos.ForeColor = Color.Black;
            Dtg_AlumnoGrupo.DataSource = null;
            Dtg_AlumnoGrupo.Refresh();
            Dtg_AlumnoGrupo.DataSource = ListaAlumnosGrupos;
            Dtg_AlumnoGrupo.Refresh();
            Dtg_AlumnoGrupo.ForeColor = Color.Black;
            Dtg_Materias.DataSource = null;
            Dtg_Materias.Refresh();
            Dtg_Materias.DataSource = ListaMaterias;
            Dtg_Materias.Refresh();
            Dtg_Materias.ForeColor = Color.Black;
            Dtg_GrupoMateria.DataSource = null;
            Dtg_GrupoMateria.Refresh();
            Dtg_GrupoMateria.DataSource = ListaGrupoMateria;
            Dtg_GrupoMateria.Refresh();
            Dtg_GrupoMateria.ForeColor = Color.Black;
            if (Dtg_Materias.RowCount == 0)
                Dtg_Alumnos.Enabled = false;
            else
                Dtg_Alumnos.Enabled = true;
        }
        private List<Alumnos> Alumnos()
        {
            string filtro= string.Format(" left join alumnogrupo on ncontrol=fkncontrol;");
            return _AlumnoManejador.ObtenerLista(filtro);
        }
        private List<Alumnos> AlumnosGrupo()
        {
            string filtro=string.Format(" right join alumnogrupo on NControl=fkncontrol and fkgrupomateria='{0}';", _AlumnosGrupo.Fkgrupomateria);
            return _AlumnoManejador.ObtenerLista(filtro);
        }
        private List<Materias> Materias()
        {
            string filtro=string.Format(" left join grupomateria on idmateria=fkmateria", _GrupoMaterias.Fkmateria);
            return _MateriaManejador.ObtenerLista(filtro);
        }
        private List<Materias> GrupoMaterias()
        {
            string filtro=string.Format(" right join grupomateria on idmateria=fkmateria", _GrupoMaterias.Fkmateria);
            return _MateriaManejador.ObtenerLista(filtro);
        }

        private void Dtg_Alumnos_DoubleClick(object sender, EventArgs e)
        {
            int fila = Dtg_Alumnos.CurrentRow.Index;
            IntercambioAlumno(fila,ListaGrupoMateria);
        }

        private void Dtg_AlumnoGrupo_DoubleClick(object sender, EventArgs e)
        {
            int fila = Dtg_AlumnoGrupo.CurrentRow.Index;
            RegresoAlumno(fila);
        }

        private void Dtg_Materias_DoubleClick(object sender, EventArgs e)
        {
            int fila = Dtg_Materias.CurrentRow.Index;
            IntercambioMateria(fila);
        }

        private void Dtg_GrupoMateria_DoubleClick(object sender, EventArgs e)
        {
            int fila = Dtg_GrupoMateria.CurrentRow.Index;
            RegresoMateria(fila);
        }

        private void Cmb_Grado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(_isEnableBinding)
            _Grupos.Grado = Cmb_Grupo.SelectedItem.ToString();
        }

        private void Cmb_Grupo_SelectedIndexChanged(object sender, EventArgs e)
        {
            _Grupos.Grupo = Convert.ToInt32(Cmb_Grado.SelectedItem);
        }

        private void Cmb_Carrera_SelectedIndexChanged(object sender, EventArgs e)
        {
            _Grupos.Carrera = Cmb_Carrera.SelectedItem.ToString();
        }

        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GruposModal_Load(object sender, EventArgs e)
        {
        }
        private void IntercambioAlumno(int c,List<Materias> Materias)
        {
            Alumnos _alumnos;
            AlumnosGrupo _alumnosGrupo;
            _alumnos = ListaAlumnos[c];
            foreach (Materias materia in Materias)
            {
                _alumnosGrupo = new AlumnosGrupo
                {
                    Id = "",
                    Fkncontrol = _alumnos.NControl,
                    Fkgrupomateria = string.Concat(Cmb_Grupo.Text, Cmb_Grado.Text, string.Concat(Cmb_Grupo.Text, Cmb_Grado.Text, materia.Idmateria))
                };
                CommitAlumnosGrupo.Add(_AlumnoGrupoManejador.Guardar(_alumnosGrupo));
            }
            ListaAlumnos.Remove(_alumnos);
            ListaAlumnosGrupos.Add(_alumnos);
            LlenarCombos();
        }
        private string GenerarCodigoAlumno(string Fkncontrol,string Fkgrupomateria)
        {
            string codigo;
            codigo = Fkncontrol.Substring(6, 2);
            codigo += Fkgrupomateria.Substring(4, 3);
            return codigo;
        }

        private void RegresoAlumno(int c)
        {
            Alumnos _alumnos;
            _alumnos = ListaAlumnosGrupos[c];
            ListaAlumnos.Add(_alumnos);
            ListaAlumnosGrupos.Remove(_alumnos);
            string id = GenerarCodigoAlumno(_alumnos.NControl, string.Concat(Cmb_Grupo.Text, Cmb_Grado.Text, Cmb_Carrera.Text));
           CommitAlumnosGrupo.Add(_AlumnoGrupoManejador.Eliminar(id));
            LlenarCombos();
        }
        private void IntercambioMateria(int c)
        {
            Materias _materias;
            GrupoMaterias _grupoMaterias;
            _materias = ListaMaterias[c];
            _grupoMaterias = new GrupoMaterias
            {
                Id = "",
                Fkgrupo = Convert.ToInt32(Cmb_Grado.Text),
                Fkgrado = Cmb_Grupo.Text
            };
            ListaMaterias.Remove(_materias);
            ListaGrupoMateria.Add(_materias);
            CommitGrupoMateria.Add(_GrupoMateriaManejador.Guardar(_grupoMaterias));
            LlenarCombos();
        }
        
        private void RegresoMateria(int c)
        {
            Materias _materias;
            _materias = ListaMaterias[c];
            ListaMaterias.Add(_materias);
            ListaGrupoMateria.Remove(_materias);
            CommitGrupoMateria.Add(_GrupoMateriaManejador.Eliminar(string.Concat(Cmb_Grupo.Text, Cmb_Grado.Text, _materias.Idmateria)));
            LlenarCombos();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int fila = Dtg_Alumnos.CurrentRow.Index;
            IntercambioAlumno(fila,ListaGrupoMateria);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int fila = Dtg_AlumnoGrupo.CurrentRow.Index;
            RegresoAlumno(fila);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int fila = Dtg_Materias.CurrentRow.Index;
            IntercambioMateria(fila);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int fila = Dtg_GrupoMateria.CurrentRow.Index;
            RegresoMateria(fila);
        }

        private void Dtg_Alumnos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
