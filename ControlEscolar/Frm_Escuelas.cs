﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using System.IO;

namespace ControlEscolar
{
    public partial class Frm_Escuelas : Form
    {
        Escuelas _Escuelas;
        EscuelaManejador _EscuelaManejador;
        string directorio = @"C:\ControlEscolar\Logos\";
        public Frm_Escuelas()
        {
            InitializeComponent();
            _EscuelaManejador = new EscuelaManejador();
            _Escuelas = new Escuelas();
        }
        private void BuscarEscuelas(string filtro)
        {
            Dtg_Datos.DataSource = _EscuelaManejador.ObtenerLista(filtro);
        }

        private void Btn_nuevo_Click(object sender, EventArgs e)
        {
            EscuelasModal _escuelasModal = new EscuelasModal();
            _escuelasModal.ShowDialog();
            BuscarEscuelas("");
        }

        private void Dtg_Datos_DoubleClick(object sender, EventArgs e)
        {
            BindingEscuela();
            EscuelasModal _escuelasModal = new EscuelasModal(_Escuelas);
            _escuelasModal.ShowDialog();
            BuscarEscuelas("");

        }

        private void Frm_Escuelas_Load(object sender, EventArgs e)
        {
            BuscarEscuelas("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarEscuelas(txtBuscar.Text);
        }
        private void Eliminar()
        {
            string id = Dtg_Datos.CurrentRow.Cells["clave"].Value.ToString();
            _EscuelaManejador.Eliminar(id);
            directorio += Dtg_Datos.CurrentRow.Cells["clave"].Value.ToString();
            if (Directory.Exists(directorio))
                Directory.Delete(directorio, true);
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("¿Estás seguro que deseas eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes) ;
            {
                try
                {
                    Eliminar();
                    // EliminarCarpeta();
                    BuscarEscuelas("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void BindingEscuela()
        {
            _Escuelas.Clave = Dtg_Datos.CurrentRow.Cells["Clave"].Value.ToString();
            _Escuelas.Nombre = Dtg_Datos.CurrentRow.Cells["Nombre"].Value.ToString();
            _Escuelas.Domicilio = Dtg_Datos.CurrentRow.Cells["Domicilio"].Value.ToString();
            _Escuelas.Nexterior = Dtg_Datos.CurrentRow.Cells["Nexterior"].Value.ToString();
            _Escuelas.Estado = Dtg_Datos.CurrentRow.Cells["Estado"].Value.ToString();
            _Escuelas.Municipio = Dtg_Datos.CurrentRow.Cells["Municipio"].Value.ToString();
            _Escuelas.Telefono = Dtg_Datos.CurrentRow.Cells["Telefono"].Value.ToString();
            _Escuelas.Email = Dtg_Datos.CurrentRow.Cells["Email"].Value.ToString();
            _Escuelas.Paginaweb = Dtg_Datos.CurrentRow.Cells["Paginaweb"].Value.ToString();
            _Escuelas.Director = Dtg_Datos.CurrentRow.Cells["Director"].Value.ToString();
            _Escuelas.Logo = Dtg_Datos.CurrentRow.Cells["Logo"].Value.ToString();
        }

        private void Dtg_Datos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }
    }
}
