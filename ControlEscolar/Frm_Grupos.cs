﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class Frm_Grupos : Form
    {
        Grupos _Grupos;
        GrupoManejador _GrupoManejador;
        public Frm_Grupos()
        {
            InitializeComponent();
            _Grupos = new Grupos();
            _GrupoManejador = new GrupoManejador();
        }

        private void Btn_nuevo_Click(object sender, EventArgs e)
        {
            GruposModal gruposModal = new GruposModal(_Grupos);
            gruposModal.ShowDialog();
            BuscarGrupos(_Grupos,"");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarGrupos(_Grupos,string.Format(" where carrera like '%{0}%'",txtBuscar.Text));
        }
        private void BuscarGrupos(Grupos grupos,string filtro)
        {
            Dtg_Datos.DataSource = _GrupoManejador.ObtenerLista(grupos,filtro);
        }
        private void BindingGrupo()
        {
            _Grupos.Grupo= Convert.ToInt32(Dtg_Datos.CurrentRow.Cells["Grupo"].Value);
            _Grupos.Grado = Dtg_Datos.CurrentRow.Cells["Grado"].Value.ToString();
            _Grupos.Carrera= Dtg_Datos.CurrentRow.Cells["Carrera"].Value.ToString();
        }

        private void Dtg_Datos_DoubleClick(object sender, EventArgs e)
        {
            BindingGrupo();
            GruposModal gruposModal = new GruposModal(_Grupos);
            gruposModal.ShowDialog();
            BuscarGrupos(_Grupos,"");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            _Grupos.Grupo = Convert.ToInt32(Dtg_Datos.CurrentRow.Cells["Grupo"].Value);
            _Grupos.Grado = Dtg_Datos.CurrentRow.Cells["Grado"].ToString();
            _GrupoManejador.Eliminar(_Grupos);
        }

        private void Frm_Grupos_Load(object sender, EventArgs e)
        {
            BuscarGrupos(_Grupos, "");
        }
    }
}
