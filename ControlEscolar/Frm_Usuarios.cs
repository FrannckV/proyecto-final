﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogicaNegocio.ControlEscolar;
using Enitades.ControlEscolar;

namespace ControlEscolar
{
    public partial class Frm_Usuarios : Form
    {
        UsuarioManejador _usuarioManejador;
        Usuarios _usuarios;
        public Frm_Usuarios()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuarios = new Usuarios();
        }

        private void BuscarUsuarios(string filtro)
        {
            Dtg_Datos.DataSource = _usuarioManejador.ObtenerLista(filtro);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BuscarUsuarios("");
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            BuscarUsuarios(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro que deseas eliminar este registro?", "Eliminar registro", MessageBoxButtons.YesNo) == DialogResult.Yes);
            {
                try
                {
                    Eliminar();
                    BuscarUsuarios("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void Eliminar()
        {
            int id = Convert.ToInt32(Dtg_Datos.CurrentRow.Cells["Idusuario"].Value);
            _usuarioManejador.Eliminar(id);
        }

        private void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            UsuariosModal usuariosModal = new UsuariosModal();
            usuariosModal.ShowDialog();
            BuscarUsuarios("");
        }

        private void Dtg_Datos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BindingUsuario();
            UsuariosModal usuariosModal = new UsuariosModal(_usuarios);
            usuariosModal.ShowDialog();
            BuscarUsuarios("");
        }
        private void BindingUsuario()
        {
            _usuarios.IdUsuario = Convert.ToInt32(Dtg_Datos.CurrentRow.Cells["IdUsuario"].Value);
            _usuarios.Nombre = Dtg_Datos.CurrentRow.Cells["Nombre"].Value.ToString();
            _usuarios.ApellidoPaterno = Dtg_Datos.CurrentRow.Cells["ApellidoPaterno"].Value.ToString();
            _usuarios.ApellidoMaterno= Dtg_Datos.CurrentRow.Cells["ApellidoMaterno"].Value.ToString();
        }
    }
}
