﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Enitades.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Extensions.ControlEscolar;
namespace ControlEscolar
{
    public partial class EscuelaApp : Form
    {
        private OpenFileDialog _dialogCargarLogo;
        private EscuelaDosManejador _escuelaManejador;
        private bool _isImageClear = false;
        private bool _isEnableBinding = false;
        private EscuelaDos _escuela;
        private RutaManager _rutaManager;
        public EscuelaApp()
        {
            InitializeComponent();
            _dialogCargarLogo = new OpenFileDialog();
            _rutaManager = new RutaManager(Application.StartupPath);
            _escuelaManejador = new EscuelaDosManejador(_rutaManager);
            _escuela = new EscuelaDos();
            _escuela = _escuelaManejador.GetEscuela();
            if (!string.IsNullOrEmpty(_escuela.Id.ToString()))
                LoadEntity();
            _isEnableBinding = true; //La primera vez que entra es falso
        }

        private void LoadEntity()
        {
            Txt_Nombre.Text = _escuela.Nombre;
            Txt_Director.Text = _escuela.Director;
            Pic_Logo.ImageLocation = null;
            if (!string.IsNullOrEmpty(_escuela.Logo) && !string.IsNullOrEmpty(_dialogCargarLogo.FileName))
                Pic_Logo.ImageLocation = _rutaManager.RutaLogoEscuela(_escuela);
        }

        private void Btn_AgregaLogo_Click(object sender, EventArgs e)
        {
            CargarLogo();
        }

        private void CargarLogo()
        {
            _dialogCargarLogo.Filter = "Imagen tipo(*.png)|*.png|Imagen tipo(*.jgp)|*.jpg";
            _dialogCargarLogo.Title = "Cargar un archivo de imagen";
            _dialogCargarLogo.ShowDialog();
            if (_dialogCargarLogo.FileName != "")
            {
                if (_escuelaManejador.Cargarlogo(_dialogCargarLogo.FileName))
                {
                    Pic_Logo.ImageLocation = _dialogCargarLogo.FileName;
                    _isImageClear = false;
                }
                else
                    MessageBox.Show("No se pueden cargar imágenes mayores a 5MB");
            }
        }

        private void Btn_EliminaLogo_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de eliminar el logo? Los cambios se efectuarán después de guardar", "Eliminar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                EliminarLogo();
        }

        private void EliminarLogo()
        {
            Pic_Logo.ImageLocation = null;
            _isImageClear = true;
        }

        private void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Estás seguro de eliminar el logo? Los cambios se efectuarán después de guardar", "Eliminar", MessageBoxButtons.OKCancel) == DialogResult.OK)
                this.Close();
        }

        private void Txt_Nombre_TextChanged(object sender, EventArgs e)
        {
            BindEntity();
        }

        private void Txt_Director_TextChanged(object sender, EventArgs e)
        {
            BindEntity();
        }

        private void BindEntity()
        {
            if (_isEnableBinding)
            {
                _escuela.Nombre = Txt_Nombre.Text;
                _escuela.Director = Txt_Director.Text;
            }
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            try
            {
                if (Pic_Logo.Image != null)
                {
                    if (!string.IsNullOrEmpty(_dialogCargarLogo.FileName))
                    {
                        _escuela.Logo = _escuelaManejador.GetNombreLogo(_dialogCargarLogo.FileName);
                        if (!string.IsNullOrEmpty(_escuela.Logo))
                        {
                            _escuelaManejador.GuardarLogo(_dialogCargarLogo.FileName, 1);
                            _dialogCargarLogo.Dispose(); //Libera la memoria del Pic
                        }
                    }
                }
                else
                {
                    _escuela.Logo = string.Empty; //Da un valor nulo a la variable
                }
                if (_isImageClear)
                {
                    _escuelaManejador.LimpiarDocumento(1, "png");
                    _escuelaManejador.LimpiarDocumento(1, "jpg");
                }
                _escuelaManejador.Guardar(_escuela);
            }
            catch ( Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EscuelaApp_Load(object sender, EventArgs e)
        {

        }
    }
}
