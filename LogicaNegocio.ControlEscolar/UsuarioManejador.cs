﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;
namespace LogicaNegocio.ControlEscolar
{
    public class UsuarioManejador
    {
        private UsuariosAccesoDatos _usuarioAccesoDatos;
        //ctor + tab + tab crea un constructor en automático
        public UsuarioManejador()
        {
            _usuarioAccesoDatos = new UsuariosAccesoDatos();
        }
        public void Eliminar(int idUsuario) //Revisar
        {
            _usuarioAccesoDatos.Eliminar(idUsuario);
        }

        public void Guardar(Usuarios usuario)
        {
            _usuarioAccesoDatos.Guardar(usuario);
        }

        public List<Usuarios> ObtenerLista(string filtro)
        {
            var list = new List<Usuarios>();
            list = _usuarioAccesoDatos.ObtenerLista(filtro);
            return list;
        }
        public Tuple<bool, string> EsUsuarioValido(Usuarios usuario)
        {
            string mensaje = "";
            bool valido = true;
            if (usuario.Nombre.Length == 0)
            {
                mensaje = "El nombre del usuario es necesario";
                valido = false;
            }
            else if (!NombreValido(usuario.Nombre))
            {
                mensaje = "Escribe un formato válido para el nombre.";
                valido = false;
            }
            else if (usuario.Nombre.Length > 15)
            {
                mensaje = "La longitud del nombre de usuario es máximo 15 caracteres.";
                valido = false;
            }
            if (usuario.ApellidoMaterno.Length == 0)
            {
                mensaje = "El apellido materno del usuario es necesario";
                valido = false;
            }
            else if (!ApellidoMaternoValido(usuario.ApellidoMaterno))
            {
                mensaje = "Escribe un formato válido para el apellido materno.";
                valido = false;
            }
            else if (usuario.ApellidoMaterno.Length > 15)
            {
                mensaje = "La longitud del apellido materno de usuario es máximo 15 caracteres.";
                valido = false;
            }
            if (usuario.ApellidoPaterno.Length == 0)
            {
                mensaje = "El apellido paterno del usuario es necesario";
                valido = false;
            }
            else if (!ApellidoPaternoValido(usuario.ApellidoPaterno))
            {
                mensaje = "Escribe un formato válido para el apellido paterno.";
                valido = false;
            }
            else if (usuario.ApellidoPaterno.Length > 15)
            {
                mensaje = "La longitud del nombre del apellido paterno es máximo 15 caracteres.";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[a-zA-ZñÑ\W]*$");
            var match = regex.Match(nombre);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool ApellidoPaternoValido(string apellidoPaterno)
        {
            var regex = new Regex(@"^[a-zA-ZñÑ\W]*$");
            var match = regex.Match(apellidoPaterno);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool ApellidoMaternoValido(string apellidoMaterno)
        {
            var regex = new Regex(@"^[a-zA-ZñÑ\W]*$");
            var match = regex.Match(apellidoMaterno);
            if (match.Success)
                return true;
            else
                return false;
        }
    }
}
