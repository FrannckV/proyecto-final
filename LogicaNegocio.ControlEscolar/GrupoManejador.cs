﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Enitades.ControlEscolar;
using System.IO;
using Extensions.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GrupoManejador
    {
        private GrupoAccesoDatos _GrupoAccesoDatos = new GrupoAccesoDatos();
        public void Guardar(Grupos grupo)
        {
            _GrupoAccesoDatos.Guardar(grupo);
        }
        public Grupos GetGrupos()
        {
            return _GrupoAccesoDatos.GetGrupos();
        }
        public void Eliminar(Grupos ngrupo)
        {
            _GrupoAccesoDatos.Eliminar(ngrupo);
        }
        public List<Grupos> ObtenerLista(Grupos grupos,string filtro)
        {
            var list = new List<Grupos>();
            list = _GrupoAccesoDatos.ObtenerLista(grupos,filtro);
            return list;
        }
        public Grupos VaciarGrupos(Grupos grupos)
        {
            grupos.Grupo = 0;
            grupos.Grado = null;
            grupos.Carrera = null;
            return grupos;
        }
    }
}
