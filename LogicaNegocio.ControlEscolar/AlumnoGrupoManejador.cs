﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Enitades.ControlEscolar;
using Extensions.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AlumnoGrupoManejador
    {
        private AlumnoGrupoAccesoDatos _AlumnoGrupoAccesoDatos;
        public AlumnoGrupoManejador()
        {
            _AlumnoGrupoAccesoDatos = new AlumnoGrupoAccesoDatos();
        }
        public string Guardar(AlumnosGrupo alumnosGrupo)
        {
            return _AlumnoGrupoAccesoDatos.Guardar(alumnosGrupo);
        }
        public string Eliminar(string id)
        {
            return _AlumnoGrupoAccesoDatos.Eliminar(id);
        }
        public List<AlumnosGrupo> ObtenerLista(string filtro)
        {
            var list = new List<AlumnosGrupo>();
            list = _AlumnoGrupoAccesoDatos.ObtenerLista(filtro);
            return list;
        }
        public AlumnosGrupo GetAlumnosGrupo(AlumnosGrupo alumnosGrupo)
        {
            return _AlumnoGrupoAccesoDatos.GetAlumnosGrupo(alumnosGrupo);
        }
        public string EjecutaCommit(List<string> lista)
        {
            return _AlumnoGrupoAccesoDatos.EjecutaCommit(lista);
        }
    }
}
