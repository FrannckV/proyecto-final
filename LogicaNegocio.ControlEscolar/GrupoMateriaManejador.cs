﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class GrupoMateriaManejador
    {
        private GrupoMateriaAccesoDatos _GrupoMateriaAccesoDatos;
        public GrupoMateriaManejador()
        {
            _GrupoMateriaAccesoDatos = new GrupoMateriaAccesoDatos();
        }
        public string Guardar(GrupoMaterias grupoMaterias )
        {
            return _GrupoMateriaAccesoDatos.Guardar(grupoMaterias);
        }
        public string Eliminar(string id)
        {
            return _GrupoMateriaAccesoDatos.Eliminar(id);
        }
        public List<GrupoMaterias> ObtenerLista(string filtro)
        {
            var list = new List<GrupoMaterias>();
            list = _GrupoMateriaAccesoDatos.ObtenerLista(filtro);
            return list;
        }
        public GrupoMaterias GetAlumnosGrupo(GrupoMaterias grupoMaterias)
        {
            return _GrupoMateriaAccesoDatos.GetGrupoMaterias(grupoMaterias);
        }
        public string EjecutaCommit(List<string> lista)
        {
            return _GrupoMateriaAccesoDatos.EjecutaCommit(lista);
        }
    }
}
