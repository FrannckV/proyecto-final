﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Enitades.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolar
{
    public class MaestroManejador
    {
        MaestrosAccesoDatos _MaestrosAccesoDatos;
        public MaestroManejador()
        {
            _MaestrosAccesoDatos = new MaestrosAccesoDatos();
        }
        public void Guardar(Maestros maestros, string anio)
        {
            _MaestrosAccesoDatos.Guardar(maestros);
        }
        public void Eliminar(string nControl)
        {
            _MaestrosAccesoDatos.Eliminar(nControl);
        }
        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();
            list = _MaestrosAccesoDatos.ObtenerLista(filtro);
            return list;
        }
        public Tuple<bool, string> EsMaestroValido(Maestros maestros)
        {
            string mensaje = "";
            bool valido = true;
            if (maestros.Nombre.Length == 0)
            {
                mensaje = "El nombre del maestro es necesario";
                valido = false;
            }
            else if (!NombreValido(maestros.Nombre))
            {
                mensaje = "Escribe un formato válido para el nombre.";
                valido = false;
            }
            else if (maestros.Nombre.Length > 100)
            {
                mensaje = "La longitud del nombre del maestro es máximo 100 caracteres.";
                valido = false;
            }
            else if (maestros.Apellidomaterno.Length == 0)
            {
                mensaje = "El apellido materno del maestro es necesario";
                valido = false;
            }
            else if (!ApellidoMaternoValido(maestros.Apellidomaterno))
            {
                mensaje = "Escribe un formato válido para el apellido materno.";
                valido = false;
            }
            else if (maestros.Apellidomaterno.Length > 100)
            {
                mensaje = "La longitud del apellido materno del maestro es máximo 100 caracteres.";
                valido = false;
            }
            else if (maestros.Apellidopaterno.Length == 0)
            {
                mensaje = "El apellido paterno del maestro es necesario";
                valido = false;
            }
            else if (!ApellidoPaternoValido(maestros.Apellidopaterno))
            {
                mensaje = "Escribe un formato válido para el apellido paterno.";
                valido = false;
            }
            else if (maestros.Apellidopaterno.Length > 100)
            {
                mensaje = "La longitud del apellido paterno es máximo 100 caracteres.";
                valido = false;
            }
            else if (maestros.Sexo.Length == 0)
            {
                mensaje = "El sexo del maestro es necesario";
                valido = false;
            }
            else if (maestros.FechaNacimiento.Length == 0)
            {
                mensaje = "La fecha de nacimiento del maestro es necesaria";
                valido = false;
            }
            else if (maestros.Esta.Length == 0)
            {
                mensaje = "El estado de nacimiento del maestro es necesario";
                valido = false;
            }
            else if (maestros.Municipio.Length == 0)
            {
                mensaje = "El municipio de nacimiento del maestro es necesario";
                valido = false;
            }

            if (maestros.CorreoElectronico != "")
            {
                if (maestros.CorreoElectronico.Length > 250)
                {
                    mensaje = "La longitud del correo electrónico es máximo 250 caracteres.";
                    valido = false;
                }
                else if (!CorreoElctronicoValido(maestros.CorreoElectronico))
                {
                    mensaje = "Escribe un formato válido para el correo electrónico.";
                    valido = false;
                }
            }
            if (maestros.Tarjeta != "")
            {
                if (maestros.Tarjeta.Length !=16)
                {
                    mensaje = "La longitud de la tarjeta no es válida";
                    valido = false;
                }
                else if (!TarjetaValida(maestros.Tarjeta))
                {
                    mensaje = "Escribe un formato válido para la tarjeta.";
                    valido = false;
                }
            }
            return Tuple.Create(valido, mensaje);
        }
        private bool NombreValido(string nombre)
        {
            //var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑZáéíóúAÉÍÓÚ\s]*$");
            var match = regex.Match(nombre);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool ApellidoPaternoValido(string apellidoPaterno)
        {
            // var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑZáéíóúAÉÍÓÚ\s]*$");
            var match = regex.Match(apellidoPaterno);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool ApellidoMaternoValido(string apellidoMaterno)
        {
            // var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑZáéíóúAÉÍÓÚ\s]*$");
            var match = regex.Match(apellidoMaterno);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool CorreoElctronicoValido(string correoElectronico)
        {
            var regex = new Regex(@"^[^@]+@[^@]+\.[a-zA-Z]{2,}$");
            var match = regex.Match(correoElectronico);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool TarjetaValida(string telefono)
        {
            var regex = new Regex(@"^[0-9]{16}$");
            var match = regex.Match(telefono);
            if (match.Success)
                return true;
            else
                return false;
        }
    }
}
