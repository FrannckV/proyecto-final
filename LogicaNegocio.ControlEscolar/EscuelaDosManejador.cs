﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Enitades.ControlEscolar;
using Extensions.ControlEscolar;
using System.IO;

namespace LogicaNegocio.ControlEscolar
{
   public class EscuelaDosManejador
    {
        private EscuelaDosAccesoDatos _escuelaDosAccesoDatos = new EscuelaDosAccesoDatos();
        private RutaManager _RutaManager;
        public EscuelaDosManejador(RutaManager rutaManager)
        {
            _RutaManager = rutaManager;
        }
        public void Guardar(EscuelaDos escuela)
        {
            _escuelaDosAccesoDatos.Guardar(escuela);
        }
        public EscuelaDos GetEscuela()
        {
            return _escuelaDosAccesoDatos.GetEscuela();
        }
        public bool Cargarlogo(string fileName)
        {
            var archivoNombre = new FileInfo(fileName);
            if (archivoNombre.Length > 5000000)
                return false;
            else
                return true;
        }

        public string GetNombreLogo(string fileName)
        {
            var archivonombre = new FileInfo(fileName);
            return archivonombre.Name;
        }
        public void LimpiarDocumento(int escuelaId,string tipoDocumento)
        {
            string rutaRepositorio = "";
            string extension = "";
            switch (tipoDocumento)
            {
                case "png":
                    rutaRepositorio = _RutaManager.RutaRepositoriosLogos;
                    extension = "*.png";
                    break;
                case "jpg":
                    rutaRepositorio = _RutaManager.RutaRepositoriosLogos;
                    extension = "*.jpg";
                    break;
            }
            string ruta = Path.Combine(rutaRepositorio,escuelaId.ToString());
            if (Directory.Exists(ruta))
            {
                var obtenerArchivos = Directory.GetFiles(ruta, extension);
                FileInfo archivoAnterior;
                if (obtenerArchivos.Length!=0)
                {
                    archivoAnterior = new FileInfo(obtenerArchivos[0]); //GetFile guarda en un arreglo los archivos en la carpeta y hace la acción con el archivo en el lugar 0 
                    if (archivoAnterior.Exists)
                        archivoAnterior.Delete();
                }
            }
        }
        public void GuardarLogo(string fileName, int escuelaId)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                var archivoDocument = new FileInfo(fileName);
                string ruta = Path.Combine(_RutaManager.RutaRepositoriosLogos, escuelaId.ToString());
                if (Directory.Exists(ruta))
                {
                    var obtenerArchivos = Directory.GetFiles(ruta);
                    FileInfo archivoAnterior;
                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                            archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                        }
                    }
                    else
                    {
                        _RutaManager.CrearRepositoriosEscuela(escuelaId);
                        archivoDocument.CopyTo(Path.Combine(ruta, archivoDocument.Name));
                    }
                }
            }
        }
    }
}
