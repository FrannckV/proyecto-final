﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolar
{
    public class EscuelaManejador
    {
        EscuelaAccesoDatos _EscuelaAccesoDatos;
        public EscuelaManejador()
        {
            _EscuelaAccesoDatos = new EscuelaAccesoDatos();
        }
        public void Guardar(Escuelas escuelas)
        {
            _EscuelaAccesoDatos.Guardar(escuelas);
        }
        public void Eliminar(string clave)
        {
            _EscuelaAccesoDatos.Eliminar(clave);
        }
        public List<Escuelas> ObtenerLista(string filtro)
        {
            var list = new List<Escuelas>();
            list = _EscuelaAccesoDatos.ObtenerLista(filtro);
            return list;
        }
        public Tuple<bool, string> EsEscuelaValida(Escuelas escuelas)
        {
            string mensaje = "";
            bool valido = true;
            if (escuelas.Nombre.Length == 0)
            {
                mensaje = "El nombre de la escuela es necesario";
                valido = false;
            }
            else if (escuelas.Domicilio.Length == 0)
            {
                mensaje = "La dirección de la escuela es necesaria";
                valido = false;
            }
            else if (escuelas.Nexterior.Length==0)
            {
                mensaje = "Escribe el número exterior de la escuela.";
                valido = false;
            }
            else if (escuelas.Estado.Length == 0)
            {
                mensaje = "El estado es necesario";
                valido = false;
            }
            else if (escuelas.Municipio.Length==0)
            {
                mensaje = "Escribe un formato válido para el municipio de la escuela.";
                valido = false;
            }
            else if (escuelas.Email.Length >0)
            {
                if (!CorreoElctronicoValido(escuelas.Email))
                {
                    mensaje = "Escribe un formato válido para el correo electrónico.";
                    valido = false;
                }
            }
            else if (escuelas.Telefono.Length > 0)
            {
                if (!TelefonoValido(escuelas.Telefono))
                {
                    mensaje = "Escribe un formato válido para el número teléfono.";
                    valido = false;
                }
            }
            else if (escuelas.Paginaweb.Length > 0)
            {
                if (!WebValida(escuelas.Paginaweb))
                {
                    mensaje = "Escribe un formato válido para la página web.";
                    valido = false;
                }
            }
            else if (escuelas.Director.Length == 0)
            {
                mensaje = "El director de la escuela es necesario";
                valido = false;
            }
            else if (!directorValido(escuelas.Director))
            {
                    mensaje = "Escribe un formato válido para el nombre del director.";
                    valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
        private bool directorValido(string nombre)
        {
            //var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑZáéíóúAÉÍÓÚ\s]*$");
            var match = regex.Match(nombre);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool CorreoElctronicoValido(string correoElectronico)
        {
            var regex = new Regex(@"^[^@]+@[^@]+\.[a-zA-Z]{2,}$");
            var match = regex.Match(correoElectronico);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool TelefonoValido(string telefono)
        {
            var regex = new Regex(@"^[0-9]{10}$");
            var match = regex.Match(telefono);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool numeroValido(string telefono)
        {
            var regex = new Regex(@"^[0-9]{10}$");
            var match = regex.Match(telefono);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool WebValida(string nombre)
        {
            //var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@" ^ (?: (?: https ?| ftp)://)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})
                                (?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3})
                                {2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d
                                |22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-
                                4]\d|25[0-4]))|(?:(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9
                                ]+)(?:\.(?:[a-z\x{00a1}-\x{ffff}0-9]+-?)*[a-z\x{00a1}-\x{ffff}0-9]+)*(?:\.(
                                ?:[a-z\x{00a1}-\x{ffff}]{2,})))(?::\d{2,5})?(?:/[^\s]*)?$");
            var match = regex.Match(nombre);
            if (match.Success)
                return true;
            else
                return false;
        }

    }
}
