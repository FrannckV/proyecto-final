﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class MunicipiosManejador
    {
        private MunicipiosAccesoDatos _municipiosAccesoDatos;
            
        public MunicipiosManejador()
        {
            _municipiosAccesoDatos = new MunicipiosAccesoDatos();
        }
        public List<Municipios> ObtenerLista(string filtro)
        {
            var list = new List<Municipios>();
            list = _municipiosAccesoDatos.ObtenerLista(filtro);
            return list;
        }
    }
}
