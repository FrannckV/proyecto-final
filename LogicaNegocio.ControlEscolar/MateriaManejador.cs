﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Enitades.ControlEscolar;
using Extensions.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class MateriaManejador
    {
        private MateriaAccesoDatos _MateriaAccesoDatos;
        private EjecutarCommit _EjecutarCommit;
        private Materias _Materias;
        public MateriaManejador()
        {
            _MateriaAccesoDatos = new MateriaAccesoDatos();
            _Materias = new Materias();
        }
        public void Guardar(Materias materias)
        {
            _MateriaAccesoDatos.Guardar(materias);
        }
        public void Eliminar(string idmateria)
        {
            _MateriaAccesoDatos.Eliminar(idmateria);
        }
         public List<Materias> ObtenerLista(string filtro)
         {
             var list = new List<Materias>();
             list = _MateriaAccesoDatos.ObtenerLista(filtro);
             return list;
         }
        public Materias GetMaterias()
        {
            return _MateriaAccesoDatos.getMateria(_Materias);
        }
        public Tuple<bool, string> EsMateriaValida(Materias materias)
        {
            string mensaje = "";
            bool valido = true;
            if (materias.Nombre.Length == 0)
            {
                mensaje = "El nombre de la materia es necesario";
                valido = false;
            }
            else if (!NombreValido(materias.Nombre))
            {
                mensaje = "Escribe un formato válido para el nombre.";
                valido = false;
            }
            else if (materias.Nombre.Length > 100)
            {
                mensaje = "La longitud del nombre de la materia es máximo 100 caracteres.";
                valido = false;
            }
            return Tuple.Create(valido, mensaje);
        }
        private bool NombreValido(string nombre)
        {
            //var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑ\s\W]*$");
            var match = regex.Match(nombre);
            if (match.Success)
                return true;
            else
                return false;
        }
        public string getError()
        {
            return _MateriaAccesoDatos.getError();
        }
        public Materias VaciarMaterias(Materias materias)
        {
            materias.Idmateria = null;
            materias.Nombre = null;
            materias.Anterior = null;
            materias.Siguiente = null;
            return materias;
        }
    }
}
