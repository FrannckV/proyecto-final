﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos.ControlEscolar;
using Enitades.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolar
{
    public class AlumnoManejador
    {
        private AlumnosAccesoDatos _alumnosAccesoDatos;
        public AlumnoManejador()
        {
            _alumnosAccesoDatos = new AlumnosAccesoDatos();
        }
        public void Guardar(Alumnos alumno)
        {
            _alumnosAccesoDatos.Guardar(alumno);
        }
        public void Eliminar(string nControl)
        {
            _alumnosAccesoDatos.Eliminar(nControl);
        }
        public List<Alumnos> ObtenerLista(string filtro)
        {
            var list = new List<Alumnos>();
            list = _alumnosAccesoDatos.ObtenerLista(filtro);
            return list;
        }
        public Tuple<bool, string> EsAlumnoValido(Alumnos alumnos)
        {
            string mensaje = "";
            bool valido = true;
            if (alumnos.Nombre.Length == 0)
            {
                mensaje = "El nombre del alumno es necesario";
                valido = false;
            }
            else if (!NombreValido(alumnos.Nombre))
            {
                mensaje = "Escribe un formato válido para el nombre.";
                valido = false;
            }
            else if (alumnos.Nombre.Length > 100)
            {
                mensaje = "La longitud del nombre del alumno es máximo 100 caracteres.";
                valido = false;
            }
            else if (alumnos.ApellidoMaterno.Length == 0)
            {
                mensaje = "El apellido materno del alumno es necesario";
                valido = false;
            }
            else if (!ApellidoMaternoValido(alumnos.ApellidoMaterno))
            {
                mensaje = "Escribe un formato válido para el apellido materno.";
                valido = false;
            }
            else if (alumnos.ApellidoMaterno.Length > 100)
            {
                mensaje = "La longitud del apellido materno del alumno es máximo 100 caracteres.";
                valido = false;
            }
            else if (alumnos.ApellidoPaterno.Length == 0)
            {
                mensaje = "El apellido paterno del alumno es necesario";
                valido = false;
            }
            else if (!ApellidoPaternoValido(alumnos.ApellidoPaterno))
            {
                mensaje = "Escribe un formato válido para el apellido paterno.";
                valido = false;
            }
            else if (alumnos.ApellidoPaterno.Length > 100)
            {
                mensaje = "La longitud del apellido paterno es máximo 100 caracteres.";
                valido = false;
            }
            else if (alumnos.Sexo.Length == 0)
            {
                mensaje = "El sexo del alumno es necesario";
                valido = false;
            }
            else if (alumnos.FechaNacimiento.Length == 0)
            {
                mensaje = "La fecha de nacimiento del alumno es necesaria";
                valido = false;
            }
            else if (alumnos.Estado.Length == 0)
            {
                mensaje = "El estado de nacimiento del alumno es necesario";
                valido = false;
            }
            else if (alumnos.Municipio.Length == 0)
            {
                mensaje = "El municipio de nacimiento del alumno es necesario";
                valido = false;
            }

            if (alumnos.Domicilio != "")
            {
                if (alumnos.Domicilio.Length > 250)
                {
                    mensaje = "La longitud del domicilio es máximo 250 caracteres.";
                    valido = false;
                }
            }
           if (alumnos.CorreoEletronico != "")
            {
                if (alumnos.CorreoEletronico.Length > 250)
                {
                    mensaje = "La longitud del correo electrónico es máximo 250 caracteres.";
                    valido = false;
                }
                else if (!CorreoElctronicoValido(alumnos.CorreoEletronico))
                {
                    mensaje = "Escribe un formato válido para el correo electrónico.";
                    valido = false;
                }
            }
            
           if (alumnos.Telefono!= "")
            {
                if (alumnos.Telefono.Length > 15)
                {
                    mensaje = "La longitud del teléfono es máximo 15 caracteres.";
                    valido = false;
                }
                else if (!TelefonoValido(alumnos.Telefono))
                {
                    mensaje = "Escribe un formato válido para el teléfono.";
                    valido = false;
                }
            } 
            return Tuple.Create(valido, mensaje);
        }
        private bool NombreValido(string nombre)
        {
            //var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑ\s\W]*$");
            var match = regex.Match(nombre);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool ApellidoPaternoValido(string apellidoPaterno)
        {
           // var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑ\W]*$");
            var match = regex.Match(apellidoPaterno);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool ApellidoMaternoValido(string apellidoMaterno)
        {
           // var regex = new Regex("^[A-Z]+([A-Z]+)*$");
            var regex = new Regex(@"^[a-zA-ZñÑ\s\W]*$");
            var match = regex.Match(apellidoMaterno);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool CorreoElctronicoValido(string correoElectronico)
        {
            var regex = new Regex(@"^[^@]+@[^@]+\.[a-zA-Z]{2,}$");
            var match = regex.Match(correoElectronico);
            if (match.Success)
                return true;
            else
                return false;
        }
        private bool TelefonoValido(string telefono)
        {
            var regex = new Regex(@"^[0-9]{0,15}$");
            var match = regex.Match(telefono);
            if (match.Success)
                return true;
            else
                return false;
        }

    }
}
