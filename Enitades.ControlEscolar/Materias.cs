﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class Materias
    {
        private string idmateria, nombre, anterior, siguiente;

        public string Idmateria { get => idmateria; set => idmateria = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Anterior { get => anterior; set => anterior = value; }
        public string Siguiente { get => siguiente; set => siguiente = value; }
    }
}
