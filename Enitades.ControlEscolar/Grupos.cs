﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class Grupos
    {
        private string grado, carrera;
        private int grupo;

        public string Grado { get => grado; set => grado = value; }
        public string Carrera { get => carrera; set => carrera = value; }
        public int Grupo { get => grupo; set => grupo = value; }
    }
}
