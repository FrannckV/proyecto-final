﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class EscuelaDos
    {
        string nombre, director, logo;
        int id;

        public string Nombre { get => nombre; set => nombre = value; }
        public string Director { get => director; set => director = value; }
        public string Logo { get => logo; set => logo = value; }
        public int Id { get => id; set => id = value; }
    }
}
