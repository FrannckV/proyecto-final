﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enitades.ControlEscolar
{
    public class GrupoMaterias
    {
        private string id, fkgrado, fkmateria;
        private int fkgrupo;

        public string Id { get => id; set => id = value; }
        public string Fkgrado { get => fkgrado; set => fkgrado = value; }
        public string Fkmateria { get => fkmateria; set => fkmateria = value; }
        public int Fkgrupo { get => fkgrupo; set => fkgrupo = value; }
    }
}
