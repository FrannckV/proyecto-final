﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using ConexionBd;
using System.Data;
using Extensions.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class GrupoMateriaAccesoDatos
    {
        Conexion _conexion;
        EjecutarCommit _EjecutarCommit;
        public GrupoMateriaAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
            _EjecutarCommit = new EjecutarCommit("localhost", "root", "", "escolar", 3306);
        }
        public string Guardar(GrupoMaterias grupoMaterias)
        {
            if (string.IsNullOrEmpty(grupoMaterias.Id))
            {
                grupoMaterias.Id = GenerarCodigo(grupoMaterias); //Generar control
                return String.Format("insert into grupomateria values('{0}','{1}','{2}','{3}'", grupoMaterias.Id, grupoMaterias.Fkgrupo, grupoMaterias.Fkgrado, grupoMaterias.Fkmateria);
            }
            else
            {
                return String.Format("update grupomateria set fkmateria='{0}',fkgrupo='{1}',fkgrado='{2}', where id='{3}'",
                    grupoMaterias.Fkmateria,grupoMaterias.Fkgrupo,grupoMaterias.Fkgrado,grupoMaterias.Id);
            }
        }
        private string GenerarCodigo(GrupoMaterias grupoMaterias)
        {
            string codigo;
            codigo = grupoMaterias.Fkmateria;
            codigo += grupoMaterias.Fkgrupo;
            codigo += grupoMaterias.Fkgrado;
            return codigo;
        }

        public string Eliminar(string nControl)
        {
            return string.Format("DELETE FROM grupomateria where id='{0}'", nControl);
        }

        public List<GrupoMaterias> ObtenerLista(string filtro)
        {
            var list = new List<GrupoMaterias>();
            string consulta = string.Format("SELECT * FROM grupomateria where id like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "grupomateria");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var GrupoMateria = new GrupoMaterias
                {
                    Id = row["Id"].ToString(),
                    Fkgrupo = Convert.ToInt32(row["Fkgrupo"].ToString()),
                    Fkgrado = row["Fkgrado"].ToString(),
                    Fkmateria = row["Fkmateria"].ToString(),
                };
                list.Add(GrupoMateria);
            }
            return list;
        }
        public GrupoMaterias GetGrupoMaterias(GrupoMaterias grupoMaterias)
        {
            var ds = new DataSet();
            string consulta = string.Format("SELECT * FROM grupomateria where id='{0}'", grupoMaterias.Id);
            ds = _conexion.ObtenerDatos(consulta, "grupomateria");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var materias = new GrupoMaterias();
            foreach (DataRow row in dt.Rows)
            {
                materias.Id = row["Id"].ToString();
                materias.Fkgrupo= Convert.ToInt32(row["Fkgrupo"]);
                materias.Fkgrado= row["Fkgrado"].ToString();
            }
            return materias;
        }
        public string EjecutaCommit(List<string> lista)
        {
            return _EjecutarCommit.Commit(lista);
        }
    }
}
