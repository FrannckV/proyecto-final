﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using System.Data;
using ConexionBd;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelaDosAccesoDatos
    {
        Conexion _conexion;
        public EscuelaDosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar(EscuelaDos escuelas)
        {
            if (escuelas.Id == 0)
            {
               // escuelas.Id = GenerarCodigo(escuelas); //Generar control
                string cadena = String.Format("insert into escuelados values(1,'{0}','{1}','{2}')", escuelas.Nombre, escuelas.Director, escuelas.Logo);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = String.Format("update escuelados set nombre='{0}',director='{1}'," +
               "logo='{2}' where id=1", escuelas.Nombre, escuelas.Director, escuelas.Logo);
                _conexion.EjecutarConsulta(cadena);
            }
        }
      /*  public string GenerarCodigo(EscuelaDos escuelas)
        {
            string consulta = string.Format("select clave from escuela where estado like '%{0}%' and municipio like '%{1}%' order by clave desc limit 1;", escuelas.Estado, escuelas.Municipio);
            var ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = ds.Tables[0];
            string clave = "", estado, municipio;
            estado = escuelas.Estado.Substring(0, 3);
            municipio = escuelas.Municipio.Substring(0, 3);
            foreach (DataRow row in dt.Rows)
            {
                clave = row["clave"].ToString();
            }
            if (clave == "")
                return estado + municipio + "01";
            else
            {
                var contador = Convert.ToInt32(clave.Substring(6, 2));
                contador += 1;
                clave = clave.Substring(0, 7);
                return clave + contador;
            }
        }*/

        public EscuelaDos GetEscuela()
        {
            var ds = new DataSet();
            string consulta= "SELECT * FROM escuelados";
            ds = _conexion.ObtenerDatos(consulta, "escuelados");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var escuela = new EscuelaDos();
            foreach (DataRow row in dt.Rows)
            {
                escuela.Id = Convert.ToInt32(row["Idescuela"]);
                escuela.Nombre = row["Nombre"].ToString();
                escuela.Director = row["Director"].ToString();
                escuela.Logo = row["Logo"].ToString();
            }
            return escuela;
        }
    }
}
