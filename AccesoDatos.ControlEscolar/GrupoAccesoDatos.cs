﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class GrupoAccesoDatos
    {
        Conexion _conexion;
        public GrupoAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar(Grupos grupos)
        {
            if (NuevoGrupo(grupos))
            { 
                string cadena = String.Format("insert into grupo values('{0}','{1}','{2}')", grupos.Grupo, grupos.Grado, grupos.Carrera);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = String.Format("update grupo set carrera='{0}' where ngrupo='{1}' and grado='{2}'",
                    grupos.Carrera, grupos.Grupo, grupos.Grado);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        private bool NuevoGrupo(Grupos grupos)
        {
            List<Grupos> ListaGrupos;
            ListaGrupos = ObtenerLista(grupos,string.Format("where ngrupo='{0}' and grado='{1}'",grupos.Grupo,grupos.Grado));
            if (ListaGrupos.Count >= 1)
                return false;
            else
                return true;
        }

        public void Eliminar(Grupos grupos)
        {
            string cadena = string.Format("DELETE FROM grupo where ngrupo='{0}' and grado='{1}'", grupos.Grupo, grupos.Grado);
            _conexion.EjecutarConsulta(cadena);
        }

        public List<Grupos> ObtenerLista(Grupos grupos,string filtro)
        {
            var list = new List<Grupos>();
            string consulta = string.Format("SELECT * FROM grupo {0}",filtro);
            var ds = _conexion.ObtenerDatos(consulta, "grupo");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Grupo = new Grupos
                {
                    Grupo = Convert.ToInt32(row["Ngrupo"]),
                    Grado = row["Grado"].ToString(),
                    Carrera = row["Carrera"].ToString(),
                };
                list.Add(Grupo);
            }
            return list;
        }
        public Grupos GetGrupos()
        {
            var ds = new DataSet();
            string consulta = "SELECT * FROM grupo";
            ds = _conexion.ObtenerDatos(consulta, "grupo");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var grupos = new Grupos();
            foreach (DataRow row in dt.Rows)
            {
                grupos.Grupo= Convert.ToInt32(row["Ngrupo"]);
                grupos.Grado= row["Grado"].ToString();
                grupos.Carrera= row["Carrera"].ToString();
            }
            return grupos;
        }

    }
}
