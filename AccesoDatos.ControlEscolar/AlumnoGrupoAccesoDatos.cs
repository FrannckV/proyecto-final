﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using ConexionBd;
using System.Data;
using Extensions.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class AlumnoGrupoAccesoDatos
    {
        Conexion _conexion;
        EjecutarCommit _EjecutarCommit;
        public AlumnoGrupoAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
            _EjecutarCommit = new EjecutarCommit("localhost", "root", "", "escolar", 3306);
        }
        public string Guardar(AlumnosGrupo alumnosGrupo)
        {
            if (string.IsNullOrEmpty(alumnosGrupo.Id))
            {
                alumnosGrupo.Id = GenerarCodigo(alumnosGrupo); //Generar control
                return String.Format("insert into alumnogrupo values('{0}','{1}','{2}')", alumnosGrupo.Id, alumnosGrupo.Fkncontrol, alumnosGrupo.Fkgrupomateria);
            }
            else
            {
                return String.Format("update alumnogrupo set fkcontrol='{0}',fkgrupomateria='{1}' where id='{2}'",
                    alumnosGrupo.Fkncontrol, alumnosGrupo.Fkgrupomateria, alumnosGrupo.Id);
            }
        }
        private string GenerarCodigo(AlumnosGrupo alumnosGrupo)
        {
            string codigo;
            codigo = alumnosGrupo.Fkncontrol.Substring(6,2);
            codigo += alumnosGrupo.Fkgrupomateria.Substring(4,3);
            return codigo;
        }

        public string Eliminar(string id)
        {
            return string.Format("DELETE FROM alumnogrupo where id='{0}'", id);
        }

        public List<AlumnosGrupo> ObtenerLista(string filtro)
        {
            var list = new List<AlumnosGrupo>();
            string consulta = string.Format("SELECT * FROM alumnogrupo where id like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "alumnogrupo");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var alumnosGrupo = new AlumnosGrupo
                {
                    Id = row["Id"].ToString(),
                    Fkncontrol =row["Fkncontrol"].ToString(),
                    Fkgrupomateria = row["Fkgrupomateria"].ToString(),
                };
                list.Add(alumnosGrupo);
            }
            return list;
        }
        public AlumnosGrupo GetAlumnosGrupo(AlumnosGrupo alumnosGrupo)
        {
            var ds = new DataSet();
            string consulta = string.Format("SELECT * FROM alumnogrupo where id='{0}'",alumnosGrupo.Id);
            ds = _conexion.ObtenerDatos(consulta, "alumnogrupo");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var alumnos = new AlumnosGrupo();
            foreach (DataRow row in dt.Rows)
            {
                alumnos.Id= row["Id"].ToString();
                alumnos.Fkncontrol= row["Fkncontrol"].ToString();
                alumnos.Fkgrupomateria= row["Fkgrupomateria"].ToString();
            }
            return alumnos;
        }
        public string EjecutaCommit(List<string> lista)
        {
            return _EjecutarCommit.Commit(lista);
        }
    }
}
