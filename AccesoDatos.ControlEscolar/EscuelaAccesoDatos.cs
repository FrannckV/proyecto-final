﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelaAccesoDatos
    {
        Conexion _conexion;
        public EscuelaAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar(Escuelas escuelas)
        {
            if (escuelas.Clave == null)
            {
                escuelas.Clave = GenerarCodigo(escuelas); //Generar control
                string cadena = String.Format("insert into escuela values('{0}','{1}','{2}','{3}','{4}','{5}'," +
                    "'{6}','{7}','{8}','{9}','{10}')", escuelas.Clave, escuelas.Nombre, escuelas.Domicilio, escuelas.Nexterior,
                    escuelas.Estado, escuelas.Municipio, escuelas.Telefono, escuelas.Email, escuelas.Paginaweb, escuelas.Director, escuelas.Logo);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = String.Format("update escuela set nombre='{0}',domicilio='{1}',nexterior='{2}'," +
               "estado='{3}',municipio='{4}',telefono='{5}',email='{6}',paginaweb='{7}',director='{8}'," +
               "logo='{9}' where clave='{10}'", escuelas.Nombre, escuelas.Domicilio, escuelas.Nexterior, 
               escuelas.Estado, escuelas.Municipio,escuelas.Telefono, escuelas.Email, escuelas.Paginaweb, 
               escuelas.Director, escuelas.Logo,escuelas.Clave);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public void Eliminar(string clave)
        {
            string cadena = string.Format("DELETE FROM escuela where clave='{0}'", clave);
            _conexion.EjecutarConsulta(cadena);
        }
        public string GenerarCodigo(Escuelas escuelas)
        {
            string consulta = string.Format("select clave from escuela where estado like '%{0}%' and municipio like '%{1}%' order by clave desc limit 1;",escuelas.Estado,escuelas.Municipio);
            var ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = ds.Tables[0];
            string clave = "", estado, municipio;
            estado = escuelas.Estado.Substring(0, 3);
            municipio = escuelas.Municipio.Substring(0, 3);
            foreach (DataRow row in dt.Rows)
            {
                clave = row["clave"].ToString();
            }
            if (clave == "")
                return estado + municipio + "01";
            else
            {
                var contador = Convert.ToInt32(clave.Substring(6, 2));
                contador += 1;
                clave = clave.Substring(0, 7);
                return clave + contador;
            }
        }

        public List<Escuelas> ObtenerLista(string filtro)
        {
            var list = new List<Escuelas>();
            string consulta = string.Format("SELECT * FROM escuela where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "escuela");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var escuelas = new Escuelas
                {
                    Clave = row["Clave"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Domicilio = row["Domicilio"].ToString(),
                    Nexterior = row["Nexterior"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Municipio = row["Municipio"].ToString(),
                    Telefono = row["Telefono"].ToString(),
                    Email = row["Email"].ToString(),
                    Paginaweb = row["Paginaweb"].ToString(),
                    Director = row["Director"].ToString(),
                    Logo = row["Logo"].ToString(),
                };
                list.Add(escuelas);
            }
            return list;
        }
    }
}
