﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class EstadosAccesoDatos
    {
        Conexion _conexion;
        public EstadosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public List<Estados> ObtenerLista(string filtro)
        {
            var list = new List<Estados>();
            string consulta = string.Format("SELECT * FROM estado", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "estado");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Estados = new Estados
                {
                    Codigo = row["codigo"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    };
                list.Add(Estados);
            }
            return list;
        }
    }
}
