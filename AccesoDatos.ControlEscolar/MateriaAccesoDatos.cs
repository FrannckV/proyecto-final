﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConexionBd;
using Enitades.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class MateriaAccesoDatos
    {
        Conexion _conexion;
        public MateriaAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar(Materias materias)
        {
            if (materias.Idmateria == null)
            {
                materias.Idmateria = GenerarCodigo(materias); //Generar control
                materias = revisaAnteriorSiguiente(materias);
                string cadena = String.Format("insert into materia values('{0}','{1}',{2},{3})",
                    materias.Idmateria, materias.Nombre, materias.Anterior, materias.Siguiente);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                materias = revisaAnteriorSiguiente(materias);
                string cadena = String.Format("update materia set nombre='{0}',anterior={1},siguiente={2}" +
                    " where idmateria='{3}'", materias.Nombre, materias.Anterior, materias.Siguiente,materias.Idmateria);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        private Materias revisaAnteriorSiguiente(Materias materias)
        {
            if (string.IsNullOrEmpty(materias.Anterior))
                materias.Anterior = "null";
            else
                materias.Anterior = String.Format("'{0}'",materias.Anterior);
            if (string.IsNullOrEmpty(materias.Siguiente))
                materias.Siguiente = "null";
            else
                materias.Siguiente = String.Format("'{0}'", materias.Siguiente);
            return materias;
        }
        private string GenerarCodigo(Materias materias)
        {
            string consulta = string.Format("select idmateria from materia where nombre like '%{0}%' order by idmateria desc limit 1;", materias.Nombre);
            var ds = _conexion.ObtenerDatos(consulta, "materia");
            var dt = ds.Tables[0];
            string control = "";
            foreach (DataRow row in dt.Rows)
            {
                control = row["idmateria"].ToString();
            }
            if (string.IsNullOrEmpty(control))
                return materias.Nombre.Substring(0, 3) + "00";
            else
            {
                var contador = Convert.ToInt32(control.Substring(3, 2));
                contador += 1;
                return control + "0" + contador;
            }
        }

        public void Eliminar(string idmateria)
        {
            string cadena = string.Format("DELETE FROM materia where idmateria='{0}'", idmateria);
            _conexion.EjecutarConsulta(cadena);
        }
        public string getError()
        {
            return _conexion.Error();
        }
        public Materias getMateria(Materias materias)
        {
            var ds = new DataSet();
            string consulta = string.Format("SELECT * FROM materia where idmateria={0}",materias.Idmateria);
            ds = _conexion.ObtenerDatos(consulta, "materia");
            var dt = new DataTable();
            dt = ds.Tables[0];
            var materia = new Materias();
            foreach (DataRow row in dt.Rows)
            {
                materia.Idmateria = row["Idmateria"].ToString();
                materia.Nombre = row["Nombre"].ToString();
                materia.Anterior = row["Anterior"].ToString();
                materia.Siguiente = row["Siguiente"].ToString();
            }
            return materia;
        }
         public List<Materias> ObtenerLista(string filtro)
          {
              var list = new List<Materias>();
              string consulta = string.Format("SELECT * FROM materia {0}", filtro);
              var ds = _conexion.ObtenerDatos(consulta, "materia");
              var dt = ds.Tables[0];
              foreach (DataRow row in dt.Rows)
              {
                  var Materia = new Materias
                  {
                      Idmateria = row["Idmateria"].ToString(),
                      Nombre = row["Nombre"].ToString(),
                      Anterior = row["Anterior"].ToString(),
                      Siguiente = row["Siguiente"].ToString(),
                  };
                  list.Add(Materia);
              }
              return list;
          }
    }
}
