﻿using System;
using System.Collections.Generic;
using Enitades.ControlEscolar;
using ConexionBd;
using System.Data;

namespace AccesoDatos.ControlEscolar
{
    public class MaestrosAccesoDatos
    {
        Conexion _conexion;
        public MaestrosAccesoDatos()
        {
            _conexion = new Conexion("localhost", "root", "", "escolar", 3306);
        }
        public void Guardar(Maestros maestros)
        {
            if (maestros.NControl == null)
            {
                maestros.NControl = GenerarCodigo(maestros); //Generar control
                string cadena = String.Format("insert into maestro values('{0}','{1}','{2}','{3}','{4}','{5}'," +
                    "'{6}','{7}','{8}','{9}','{10}','{11}','{12}')", maestros.NControl, maestros.Nombre, maestros.Apellidopaterno,
                    maestros.Apellidomaterno, maestros.FechaNacimiento, maestros.Esta, maestros.Municipio,
                    maestros.Sexo, maestros.CorreoElectronico, maestros.Tarjeta, maestros.DocTitulo, maestros.DocMaestria, maestros.DocDoctorado);
                _conexion.EjecutarConsulta(cadena);
            }
            else
            {
                string cadena = String.Format("update maestro set nombre='{0}',apellidopaterno='{1}',apellidomaterno='{2}'," +
               "fechanacimiento='{3}',estado='{4}',municipio='{5}',sexo='{6}',correoElectronico='{7}',tarjeta='{8}'," +
               "docTitulo='{9}', docMaestria='{10}', docDoctorado='{11}' where NControl='{12}'", maestros.Nombre, maestros.Apellidopaterno,
               maestros.Apellidomaterno, maestros.FechaNacimiento, maestros.Esta, maestros.Municipio,
               maestros.Sexo, maestros.CorreoElectronico, maestros.Tarjeta, maestros.DocTitulo, maestros.DocMaestria, maestros.DocDoctorado, maestros.NControl);
                _conexion.EjecutarConsulta(cadena);
            }
        }
        public void Eliminar(string nControl)
        {
            string cadena = string.Format("DELETE FROM maestro where NControl='{0}'", nControl);
            _conexion.EjecutarConsulta(cadena);
        }
        public string GenerarCodigo(Maestros maestros)
        {
            string filtro = maestros.Anio;
            string consulta = string.Format("select ncontrol as anio from maestro where ncontrol like '%{0}%' order by nControl desc limit 1;", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "maestro");
            var dt = ds.Tables[0];
            string anio = "LD";
            string control="";
            foreach (DataRow row in dt.Rows)
            {
                control = row["anio"].ToString();
            }
            if (control == "")
                return anio + maestros.Anio + "01";
            else
            {
                var contador = Convert.ToInt32(control.Substring(2,6));
                contador+=1;
                return anio + contador;
            }
        }

        public List<Maestros> ObtenerLista(string filtro)
        {
            var list = new List<Maestros>();
            string consulta = string.Format("SELECT * FROM maestro where nombre like '%{0}%'", filtro);
            var ds = _conexion.ObtenerDatos(consulta, "maestro");
            var dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var Maestro = new Maestros
                {
                    NControl = row["NControl"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    Apellidopaterno = row["Apellidopaterno"].ToString(),
                    Apellidomaterno = row["Apellidomaterno"].ToString(),
                    FechaNacimiento = row["FechaNacimiento"].ToString(),
                    Esta = row["Estado"].ToString(),
                    Municipio = row["Municipio"].ToString(),
                    Sexo = row["Sexo"].ToString(),
                    CorreoElectronico = row["CorreoElectronico"].ToString(),
                    Tarjeta = row["Tarjeta"].ToString(),
                    DocTitulo = row["DocTitulo"].ToString(),
                    DocMaestria = row["DocMaestria"].ToString(),
                    DocDoctorado = row["DocDoctorado"].ToString(),
                };
                list.Add(Maestro);
            }
            return list;
        }
    }
}
