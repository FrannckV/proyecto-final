﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enitades.ControlEscolar;
using ConexionBd;
using MySql.Data.MySqlClient;

namespace Extensions.ControlEscolar
{
    public class EjecutarCommit
    {
        MySqlConnection _conn;
        public EjecutarCommit(string server, string user, string password, string database, uint port)
        {
            MySqlConnectionStringBuilder cadena = new MySqlConnectionStringBuilder();
            cadena.Server = server;
            cadena.UserID = user;
            cadena.Password = password;
            cadena.Database = database;
            cadena.Port = port;
            _conn = new MySqlConnection(cadena.ToString());
        }
        public string Commit(List<string> lista)
        {
            MySqlCommand command = _conn.CreateCommand();
            MySqlTransaction transaction;
            _conn.Open();
            transaction = _conn.BeginTransaction();
            command.Connection = _conn;
            command.Transaction = transaction;
            try
            {
                foreach (string comando in lista)
                {
                    command.CommandText = comando;
                    transaction.Commit();
                }
                return ("Transacción correcta.");
            }
            catch (Exception ex)
            {
                string error;
                error = string.Format("Commit Exception Type: {0}\nMessage: {0}", ex.GetType(), ex.Message);
                try
                {
                    transaction.Rollback();
                }
                catch (Exception ex2)
                {
                    error += string.Format("\nRollback Exception Type: {0}\nMessage: {0}", ex2.GetType(), ex2.Message);
                }
                return error;
            }
        }
        /* public string CommitAlumno(List<AlumnosGrupo> alumnos, bool agregar)
         {
             MySqlCommand command = _conn.CreateCommand();
             MySqlTransaction transaction;
             _conn.Open();
             transaction = _conn.BeginTransaction();
             command.Connection = _conn;
             command.Transaction = transaction;
             try
             {
                 if (agregar)
                     AgregarAlumno(alumnos, command);
                 else
                     EliminarAlumno(alumnos, command);
                 transaction.Commit();
                 return ("Transacción correcta.");
             }
             catch (Exception ex)
             {
                 string error;
                 error = string.Format("Commit Exception Type: {0}\nMessage: {0}", ex.GetType(), ex.Message);
                 try
                 {
                     transaction.Rollback();
                 }
                 catch (Exception ex2)
                 {
                     error += string.Format("\nRollback Exception Type: {0}\nMessage: {0}", ex2.GetType(), ex2.Message);
                 }
                 return error;
             }
         }*/

    }
}
