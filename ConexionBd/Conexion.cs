﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows;


namespace ConexionBd
{
    public class Conexion
    {
        MySqlConnection _conn;
        private static string error;
        public void Abrir()
        {
            _conn.Open();
        }
        public void Cerrar()
        {
            _conn.Close();
        }
        public Conexion(string server, string user, string password, string database, uint port)
        {
            MySqlConnectionStringBuilder cadena = new MySqlConnectionStringBuilder();
            cadena.Server = server;
            cadena.UserID = user;
            cadena.Password = password;
            cadena.Database = database;
            cadena.Port = port;
            _conn = new MySqlConnection(cadena.ToString());
        }
        public void EjecutarConsulta(string cadena)
        {
            try
            {
                _conn.Open();
                MySqlCommand cmm = new MySqlCommand(cadena, _conn);
                cmm.ExecuteNonQuery();
                _conn.Close();
                error = Error(false);
            }
            catch (Exception ex)
            {
                error = Error(true);
                _conn.Close();
            }
        }
        private string Error(bool error)
        {
            if (error)
                return "Error al intentar eliminar. El registro está referenciado a sí mismo o a otro registro.";
            else
                return "";
        }
        public string Error()
        {
            return error;
        }
        public DataSet ObtenerDatos(string cadena, string tabla)
        {
            _conn.Open();
            var ds = new DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(cadena, _conn);
            da.Fill(ds, tabla);
            _conn.Close();
            return ds;
        }
    }
}
